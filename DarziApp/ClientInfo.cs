﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DarziApp
{
    public partial class ClientInfo : Form
    {
        private PrintDocument printDocument1 = new PrintDocument();

        public ClientInfo()
        {
            InitializeComponent();
        }

        private void btn_checkOrder_Click(object sender, EventArgs e)
        {
            if (inp_name.Text == "" || !inp_ph.MaskFull || inp_quantity.Value == 0 || inp_pay.Value == 0)
            {
                MessageBox.Show("Please Enter All Information");
            }
            else
            {
                StaticValuesHolder.NAME = inp_name.Text;
                StaticValuesHolder.PHONE = inp_ph.Text.ToString();
                StaticValuesHolder.SUITQUANTITY = inp_quantity.Value.ToString();
                StaticValuesHolder.TOTALPAYMENT = inp_pay.Value.ToString();
                StaticValuesHolder.ADVANCEPAYMENT = inp_adv.Value.ToString();
                StaticValuesHolder.ORDERDATE = inp_orddate.Text;
                StaticValuesHolder.DUEDATE = inp_ddate.Text;

               
                if ((new PrintPage(false)).ShowDialog() == DialogResult.OK)
                {
                    this.Close();
                }

            }
            

            //MessageBox.Show(StaticValuesHolder.DESCRIPTION);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            CaptureScreen();
            printDocument1.Print();
        }


        

        Bitmap memoryImage;

        private void CaptureScreen()
        {
            Graphics myGraphics = this.CreateGraphics();
            Size s = this.Size;
            memoryImage = new Bitmap(s.Width, s.Height, myGraphics);
            Graphics memoryGraphics = Graphics.FromImage(memoryImage);
            memoryGraphics.CopyFromScreen(this.Location.X, this.Location.Y, 0, 0, s);
        }

        private void printDocument1_PrintPage(System.Object sender,
               System.Drawing.Printing.PrintPageEventArgs e)
        {
            e.Graphics.DrawImage(memoryImage, 0, 0);
        }

        


    }
}

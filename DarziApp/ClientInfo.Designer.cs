﻿namespace DarziApp
{
    partial class ClientInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.inp_name = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.inp_quantity = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.inp_pay = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.inp_adv = new System.Windows.Forms.NumericUpDown();
            this.inp_ph = new System.Windows.Forms.MaskedTextBox();
            this.inp_orddate = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.inp_ddate = new System.Windows.Forms.DateTimePicker();
            this.btn_checkOrder = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.inp_quantity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inp_pay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inp_adv)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label1.Location = new System.Drawing.Point(18, 91);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Name:";
            // 
            // inp_name
            // 
            this.inp_name.Location = new System.Drawing.Point(134, 88);
            this.inp_name.Name = "inp_name";
            this.inp_name.Size = new System.Drawing.Size(139, 20);
            this.inp_name.TabIndex = 9;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label2.Location = new System.Drawing.Point(17, 116);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Phone Number:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label3.Location = new System.Drawing.Point(17, 142);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Suit Quantity:";
            // 
            // inp_quantity
            // 
            this.inp_quantity.Location = new System.Drawing.Point(134, 140);
            this.inp_quantity.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.inp_quantity.Name = "inp_quantity";
            this.inp_quantity.Size = new System.Drawing.Size(138, 20);
            this.inp_quantity.TabIndex = 11;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label4.Location = new System.Drawing.Point(18, 168);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Total Payment:";
            // 
            // inp_pay
            // 
            this.inp_pay.Location = new System.Drawing.Point(135, 166);
            this.inp_pay.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.inp_pay.Name = "inp_pay";
            this.inp_pay.Size = new System.Drawing.Size(138, 20);
            this.inp_pay.TabIndex = 12;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label5.Location = new System.Drawing.Point(18, 194);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Advance:";
            // 
            // inp_adv
            // 
            this.inp_adv.Location = new System.Drawing.Point(135, 192);
            this.inp_adv.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.inp_adv.Name = "inp_adv";
            this.inp_adv.Size = new System.Drawing.Size(138, 20);
            this.inp_adv.TabIndex = 13;
            // 
            // inp_ph
            // 
            this.inp_ph.Location = new System.Drawing.Point(134, 113);
            this.inp_ph.Mask = "0000-0000000";
            this.inp_ph.Name = "inp_ph";
            this.inp_ph.Size = new System.Drawing.Size(139, 20);
            this.inp_ph.TabIndex = 10;
            this.inp_ph.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // inp_orddate
            // 
            this.inp_orddate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.inp_orddate.Location = new System.Drawing.Point(172, 224);
            this.inp_orddate.Name = "inp_orddate";
            this.inp_orddate.Size = new System.Drawing.Size(101, 20);
            this.inp_orddate.TabIndex = 14;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(96, 227);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(73, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Order Date:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(96, 253);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 13);
            this.label7.TabIndex = 5;
            this.label7.Text = "Due Date:";
            // 
            // inp_ddate
            // 
            this.inp_ddate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.inp_ddate.Location = new System.Drawing.Point(172, 250);
            this.inp_ddate.Name = "inp_ddate";
            this.inp_ddate.Size = new System.Drawing.Size(101, 20);
            this.inp_ddate.TabIndex = 15;
            // 
            // btn_checkOrder
            // 
            this.btn_checkOrder.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btn_checkOrder.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_checkOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_checkOrder.Location = new System.Drawing.Point(91, 295);
            this.btn_checkOrder.Name = "btn_checkOrder";
            this.btn_checkOrder.Size = new System.Drawing.Size(100, 23);
            this.btn_checkOrder.TabIndex = 16;
            this.btn_checkOrder.TabStop = false;
            this.btn_checkOrder.Text = "Check Order";
            this.btn_checkOrder.UseVisualStyleBackColor = false;
            this.btn_checkOrder.Click += new System.EventHandler(this.btn_checkOrder_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panel2.Controls.Add(this.panel1);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Location = new System.Drawing.Point(-1, -3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(293, 69);
            this.panel2.TabIndex = 15;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::DarziApp.Properties.Resources.logo;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel1.Location = new System.Drawing.Point(12, 7);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(71, 55);
            this.panel1.TabIndex = 16;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(100, 12);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(181, 37);
            this.label9.TabIndex = 15;
            this.label9.Text = "New Order";
            // 
            // ClientInfo
            // 
            this.AcceptButton = this.btn_checkOrder;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::DarziApp.Properties.Resources.diag;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(292, 330);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.btn_checkOrder);
            this.Controls.Add(this.inp_ddate);
            this.Controls.Add(this.inp_orddate);
            this.Controls.Add(this.inp_ph);
            this.Controls.Add(this.inp_adv);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.inp_pay);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.inp_quantity);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.inp_name);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "ClientInfo";
            this.ShowInTaskbar = false;
            this.Text = "ClientInfo";
            ((System.ComponentModel.ISupportInitialize)(this.inp_quantity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inp_pay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inp_adv)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox inp_name;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown inp_quantity;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown inp_pay;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown inp_adv;
        private System.Windows.Forms.MaskedTextBox inp_ph;
        private System.Windows.Forms.DateTimePicker inp_orddate;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker inp_ddate;
        private System.Windows.Forms.Button btn_checkOrder;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label9;
    }
}
﻿namespace DarziApp
{
    partial class MessageBoxCustom
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.getsize = new System.Windows.Forms.Button();
            this.cancel = new System.Windows.Forms.Button();
            this.print = new System.Windows.Forms.Button();
            this.delete = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(203, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "What Do You Want To Do?";
            // 
            // getsize
            // 
            this.getsize.Location = new System.Drawing.Point(12, 109);
            this.getsize.Name = "getsize";
            this.getsize.Size = new System.Drawing.Size(200, 23);
            this.getsize.TabIndex = 1;
            this.getsize.Text = "Get Size";
            this.getsize.UseVisualStyleBackColor = true;
            this.getsize.Click += new System.EventHandler(this.getsize_Click);
            // 
            // cancel
            // 
            this.cancel.Location = new System.Drawing.Point(12, 167);
            this.cancel.Name = "cancel";
            this.cancel.Size = new System.Drawing.Size(200, 23);
            this.cancel.TabIndex = 1;
            this.cancel.Text = "Cancel";
            this.cancel.UseVisualStyleBackColor = true;
            this.cancel.Click += new System.EventHandler(this.button1_Click);
            // 
            // print
            // 
            this.print.Location = new System.Drawing.Point(12, 80);
            this.print.Name = "print";
            this.print.Size = new System.Drawing.Size(200, 23);
            this.print.TabIndex = 1;
            this.print.Text = "Print It";
            this.print.UseVisualStyleBackColor = true;
            this.print.Click += new System.EventHandler(this.print_Click);
            // 
            // delete
            // 
            this.delete.Location = new System.Drawing.Point(12, 138);
            this.delete.Name = "delete";
            this.delete.Size = new System.Drawing.Size(200, 23);
            this.delete.TabIndex = 1;
            this.delete.Text = "Delete";
            this.delete.UseVisualStyleBackColor = true;
            this.delete.Click += new System.EventHandler(this.delete_Click);
            // 
            // MessageBoxCustom
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(226, 207);
            this.Controls.Add(this.print);
            this.Controls.Add(this.delete);
            this.Controls.Add(this.cancel);
            this.Controls.Add(this.getsize);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "MessageBoxCustom";
            this.Text = "MessageBox";
            this.Load += new System.EventHandler(this.MessageBox_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button getsize;
        private System.Windows.Forms.Button cancel;
        private System.Windows.Forms.Button print;
        private System.Windows.Forms.Button delete;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DarziApp
{
    public partial class PrintPage : Form
    {
        SqlConnection con = new SqlConnection(StaticValuesHolder.CONNECTIONQUERY);
        Boolean rePrint = false;
        Double size = 10F;
        public PrintPage(Boolean rePrint)
        {
            InitializeComponent();
            //StaticValuesHolder.ORDERNUMBER = DateTime.Now.ToString("MMddHHss");
            printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.PrintedPage);
            this.rePrint = rePrint;




            try
            {
                con.Close();
                con.Open();
                SqlCommand cmd = new SqlCommand("SELECT COUNT(ID) FROM Orders;", con);
                Int32 count = (Int32)cmd.ExecuteScalar();
                //MessageBox.Show(count+"");
                String orderNumber;
                if (count < 100)
                {
                    orderNumber = "00" + (count + 1);
                }
                else
                {
                    orderNumber = "" + (count + 1);
                }
                StaticValuesHolder.ORDERNUMBER = orderNumber;
            }
            catch (Exception)
            {

                throw;
            }
            finally {
                con.Close();
            }
                    









        }

        Bitmap bitmap;
        private void PrintPage_Load(object sender, EventArgs e)
        {


            String[] sizes;

            string L, S, A, C, W, D, L1, B;
            if (rePrint)
            {
             sizes = StaticValuesHolder.SIZE.Split(new String[] { "\n" }, StringSplitOptions.None);
            }
            else
            {
                sizes = StaticValuesHolder.SIZE.Split(new String[] { "/ " }, StringSplitOptions.None);
            }
            //MessageBox.Show(sizes[0].Replace("B:", "") + "-");


            B = (sizes[0].Replace("B:", ""));
            L1 = (sizes[1].Replace("L1:", ""));
            D = (sizes[2].Replace("D:", ""));
            W = (sizes[3].Replace("W:", ""));
            C = (sizes[4].Replace("C:", ""));
            A = (sizes[5].Replace("A:", ""));
            S = (sizes[6].Replace("S:", ""));
            L = (sizes[7].Replace("L:", ""));

            string printsize = String.Format("L:{0} \nS:{1} \nA:{2} \nC:{3} \nW:{4} \nD:{5} \nL1:{6} \nB:{7} \n",
                L, S, A, C, W, D, L1, B);
            lbl_size.Text = printsize;
            
            string desc;
            desc = StaticValuesHolder.DESCRIPTION;
            desc.Replace("/ ", "\n");
            lbl_desc.Text = desc.Replace("/ ", "\n");


            lbl_name.Text = StaticValuesHolder.NAME;
            lbl_qty.Text = StaticValuesHolder.SUITQUANTITY;
            lbl_date.Text = StaticValuesHolder.ORDERDATE;
            lbl_ddate.Text = StaticValuesHolder.DUEDATE;
            lbl_order.Text = StaticValuesHolder.ORDERNUMBER;
            lbl_adv.Text = StaticValuesHolder.ADVANCEPAYMENT;
            lbl_tpay.Text = StaticValuesHolder.TOTALPAYMENT;
            lbl_ph.Text = StaticValuesHolder.PHONE;

            lbl_namec.Text = StaticValuesHolder.NAME;
            lbl_qtyc.Text = StaticValuesHolder.SUITQUANTITY;
            lbl_datec.Text = StaticValuesHolder.ORDERDATE;
            lbl_ddatec.Text = StaticValuesHolder.DUEDATE;
            lbl_orderc.Text = StaticValuesHolder.ORDERNUMBER;
            lbl_advc.Text = StaticValuesHolder.ADVANCEPAYMENT;
            lbl_tpayc.Text = StaticValuesHolder.TOTALPAYMENT;
            lbl_phc.Text = StaticValuesHolder.PHONE;


            ////Add a Panel control.
            //Panel panel = new Panel();
            //this.Controls.Add(panel);

            ////Create a Bitmap of size same as that of the Form.
            //Graphics grp = panel.CreateGraphics();
            //Size formSize = this.ClientSize;
            //bitmap = new Bitmap(formSize.Width, formSize.Height, grp);
            //grp = Graphics.FromImage(bitmap);

            ////Copy screen area that that the Panel covers.
            //Point panelLocation = PointToScreen(panel.Location);
            //grp.CopyFromScreen(panelLocation.X, panelLocation.Y, 0, 0, formSize);

            ////Show the Print Preview Dialog.
            //printPreviewDialog1.Document = printDocument1;
            //printPreviewDialog1.PrintPreviewControl.Zoom = 1.5;
            //printPreviewDialog1.ShowDialog();


            //try
            //{
            //    this.Cursor = Cursors.WaitCursor;

            //    PrintForm pf = new PrintForm(this);
            //    pf.Print();
            //    this.Close();
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(this, ex.Message);
            //}
            //finally
            //{
            //    this.Cursor = Cursors.Default;
            //}
        }
        private void PrintedPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            //Print the contents.
            e.Graphics.DrawImage(bitmap, 0, 0);
        }
        private void printPreviewDialog1_Load(object sender, EventArgs e)
        {

        }

        private void PrintPage_Shown(object sender, EventArgs e)
        {
           



        }

        private void PrintPage_Validated(object sender, EventArgs e)
        {
            
            
           
        }

        private void button2_Click(object sender, EventArgs e)
        {




            button2.Visible = false;
            button3.Visible = false;

            if (!rePrint)
            {


                try
                {
                    con.Close();
                    con.Open();
                    int result = (new SqlCommand(StaticValuesHolder.GetPreparedQuery(), con)).ExecuteNonQuery();
                    if (result > 0)
                    {
                        if (MessageBox.Show("Order Has Been Placed, Do You Want To Print Receipt?", "Success Full Entry", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                        {
                            //Add a Panel control.
                            Panel panel = new Panel();
                            this.Controls.Add(panel);

                            //Create a Bitmap of size same as that of the Form.
                            Graphics grp = panel.CreateGraphics();
                            Size formSize = this.ClientSize;
                            bitmap = new Bitmap(formSize.Width, formSize.Height, grp);
                            grp = Graphics.FromImage(bitmap);

                            //Copy screen area that that the Panel covers.
                            Point panelLocation = PointToScreen(panel.Location);
                            grp.CopyFromScreen(panelLocation.X, panelLocation.Y, 0, 0, formSize);

                            //Show the Print Preview Dialog.
                            printPreviewDialog1.Document = printDocument1;
                            printPreviewDialog1.PrintPreviewControl.Zoom = 0.75;
                            printPreviewDialog1.ShowDialog();
                            this.Close();

                        }

                        this.DialogResult = DialogResult.OK;
                        this.Close();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    con.Close();
                }

            }
            else
            {
                //Add a Panel control.
                Panel panel = new Panel();
                this.Controls.Add(panel);

                //Create a Bitmap of size same as that of the Form.
                Graphics grp = panel.CreateGraphics();
                Size formSize = this.ClientSize;
                bitmap = new Bitmap(formSize.Width, formSize.Height, grp);
                grp = Graphics.FromImage(bitmap);

                //Copy screen area that that the Panel covers.
                Point panelLocation = PointToScreen(panel.Location);
                grp.CopyFromScreen(panelLocation.X, panelLocation.Y, 0, 0, formSize);

                //Show the Print Preview Dialog.
                printPreviewDialog1.Document = printDocument1;
                printPreviewDialog1.PrintPreviewControl.Zoom = 0.75;
                printPreviewDialog1.ShowDialog();
                this.Close();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            // this.lbl_size.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            if (size <= 14F)
            {
             //   size++;
               // lbl_size.Size = size;
                //this.lbl_size.Font = new System.Drawing.Font("Microsoft Sans Serif", size, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DarziApp
{
   static class StaticValuesHolder
    {
        // INSERT INTO [dbo].[Orders] ( [ordernumber], [phonenumber], [suitquatity], [totalpayment], [advancepayment], [size], [description], [orderdate], [duedate], [name]) VALUES ( N'{0}', N'{1}', N'{2}', N'{3}', N'{4}', N'{5}', N'{6}', N'{7}', N'{8}', N'{9}')

        public static string SIZE;
        public static string DESCRIPTION;
        public static string PHONE;
        public static string SUITQUANTITY;
        public static string TOTALPAYMENT;
        public static string ADVANCEPAYMENT;
        public static string ORDERDATE;
        public static string DUEDATE;
        public static string ORDERNUMBER;
        public static string NAME;
        public static string CONNECTIONQUERY = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=|DataDirectory|\Database1.mdf;Integrated Security=True;Connect Timeout=30";
            //ConfigurationManager.ConnectionStrings[1].ConnectionString;

            //"Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\\MainDatabase.mdf;Integrated Security = True";
        public static string GetPreparedQuery()
        {
            return String.Format("INSERT INTO [dbo].[Orders] ( [ordernumber], [phonenumber], [suitquatity], [totalpayment], [advancepayment], [size], [description], [orderdate], [duedate], [name]) VALUES ( N'{0}', N'{1}', N'{2}', N'{3}', N'{4}', N'{5}', N'{6}', N'{7}', N'{8}', N'{9}')",
                                ORDERNUMBER,PHONE,SUITQUANTITY,TOTALPAYMENT,ADVANCEPAYMENT,SIZE,DESCRIPTION,ORDERDATE,DUEDATE,NAME);
        }

    }
}

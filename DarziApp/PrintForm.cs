﻿using System;
using System.Drawing;
using System.Drawing.Printing;
using System.Windows.Forms;

namespace DarziApp
{
    /// <summary>
    /// A class containing methods for printing a form.
    /// </summary>
    public class PrintForm 
    {
        #region Class Variables

        private Form _frm = null;
        private int _iCurrentPageIndex = 0;

        #endregion

        #region Constructors

        /// <summary>
        /// Creates an empty PrintForm object.
        /// </summary>
        public PrintForm()
        {
        }

        /// <summary>
        /// Creates a PrintForm object initialized with the specified Form.
        /// </summary>
        public PrintForm(Form frm)
        {
            _frm = frm;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Prints the entire form to a PrintPreview object.
        /// </summary>
        public void Print()
        {
            Print(_frm);
        }

        /// <summary>
        /// Prints the entire specified form to a PrintPreview object.
        /// </summary>
        /// <param name="frm">
        /// The Form that will be printed.
        /// </param>
        public void Print(Form frm)
        {
            if (frm == null)
                return;

            // Setup a PrintDocument
            PrintDocument pd = new PrintDocument();
            pd.BeginPrint += new PrintEventHandler(this.PrintDocument_BeginPrint);
            pd.PrintPage += new PrintPageEventHandler(this.PrintDocument_PrintPage);

            // Setup & show the PrintPreviewDialog
            PrintPreviewDialog ppd = new PrintPreviewDialog();
            ppd.Document = pd;
            ppd.ShowDialog();
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Calculates pagination based on the contents of the form for the specified margins.
        /// </summary>
        /// <remarks>
        /// Each point represents the upper-left corner to where the printing rectangle
        /// will be set
        /// </remarks>
        private Point[] GeneratePrintingOffsets(Rectangle rMargins)
        {
            // Setup the array of Points
            int x = (int)Math.Ceiling((double)(_frm.Width) / (double)(rMargins.Width));
            int y = (int)Math.Ceiling((double)(_frm.Height) / (double)(rMargins.Height));
            Point[] arrPoint = new Point[x * y];

            // Fill the array
            for (int i = 0; i < y; i++)
                for (int j = 0; j < x; j++)
                    arrPoint[i * x + j] = new Point(j * rMargins.Width, i * rMargins.Height);

            return arrPoint;
        }

        /// <summary>
        /// Draws the Form on the specified Graphics object at the specified offset.
        /// </summary>
        private void DrawForm(Graphics g, Point ptOffset)
        {
            // Calculate the Title Bar rectangle
            int iBarHeight = (int)g.MeasureString(_frm.Text, _frm.Font).Height;
            Rectangle rTitleBar = new Rectangle(ptOffset.X, ptOffset.Y, _frm.Width, iBarHeight + 2);

            // Draw the rest of the Form under the Title Bar
            ptOffset.Offset(0, rTitleBar.Height);
            g.FillRectangle(new SolidBrush(_frm.BackColor), ptOffset.X, ptOffset.Y, _frm.Width, _frm.Height);

            // Draw the rest of the controls
            DrawControl(_frm, ptOffset, g);

            // Draw the Form's Title Bar
            Bitmap bmp = Bitmap.FromHicon(_frm.Icon.Handle);
            g.FillRectangle(new SolidBrush(SystemColors.ActiveCaption), rTitleBar);
            g.DrawImage(bmp,
                rTitleBar.X,
                rTitleBar.Y,
                rTitleBar.Height,
                rTitleBar.Height);
            g.DrawString(_frm.Text,
                _frm.Font,
                new SolidBrush(SystemColors.ActiveCaptionText),
                rTitleBar.X + rTitleBar.Height, // adding the width of the icon
                rTitleBar.Y + (rTitleBar.Height / 2) - (g.MeasureString(_frm.Text, _frm.Font).Height) / 2);

            // Draw the Title Bar buttons
            Size s = new Size(16, 14); // size determined from graphics program
            ControlPaint.DrawCaptionButton(g,
                ptOffset.X + _frm.Width - s.Width,
                ptOffset.Y + (rTitleBar.Height / 2) - (s.Height / 2) - rTitleBar.Height,
                s.Width,
                s.Height,
                CaptionButton.Close,
                ButtonState.Normal);
            ControlPaint.DrawCaptionButton(g,
                ptOffset.X + _frm.Width - (s.Width * 2) - 1,
                ptOffset.Y + (rTitleBar.Height / 2) - (s.Height / 2) - rTitleBar.Height,
                s.Width,
                s.Height,
                (_frm.WindowState == FormWindowState.Maximized ? CaptionButton.Restore : CaptionButton.Maximize),
                ButtonState.Normal);
            ControlPaint.DrawCaptionButton(g,
                ptOffset.X + _frm.Width - (s.Width * 3 - 1),
                ptOffset.Y + (rTitleBar.Height / 2) - (s.Height / 2) - rTitleBar.Height,
                s.Width,
                s.Height,
                CaptionButton.Minimize,
                ButtonState.Normal);

            // Draw a rectangle around the entire Form
            g.DrawRectangle(Pens.Black, ptOffset.X, ptOffset.Y - rTitleBar.Height, _frm.Width, _frm.Height + rTitleBar.Height);
        }

        /// <summary>
        /// Recursively draws the contents of the specified Form to the
        /// specified graphics object using the specified offset location.
        /// </summary>
        /// <param name="ptOffset">
        /// The point at which drawing will start.  This is used as an offset
        /// during recursion to draw child controls.  Everything within this
        /// control is drawn relative to this point.
        /// </param>
        private void DrawControl(Control ctl, Point ptOffset, Graphics g)
        {
            // Cycle through each control on the form and paint it on the graphics object
            foreach (Control c in ctl.Controls)
            {
                // Skip invisible controls
                if (!c.Visible)
                    continue;

                // Calculate the location offset for the control - this offset is
                // relative to the original offset passed in
                Point p = new Point(c.Left, c.Top);
                p.Offset(ptOffset.X, ptOffset.Y);

                // Draw the control
                if (c is GroupBox)
                    DrawGroupBox((GroupBox)c, p, g);
                else if (c is Button)
                    DrawButton((Button)c, p, g);
                else if (c is TextBox)
                    DrawTextBox((TextBox)c, p, g);
                else if (c is CheckBox)
                    DrawCheckBox((CheckBox)c, p, g);
                else if (c is Label)
                    DrawLabel((Label)c, p, g);
                else if (c is ComboBox)
                    DrawComboBox((ComboBox)c, p, g);
                else if (c is RadioButton)
                    DrawRadioButton((RadioButton)c, p, g);
                else
                    return;

                // Draw the controls within this control
                DrawControl(c, p, g);
            }
        }

        /// <summary>
        /// Draws the specified GroupBox on the specified graphics object at the
        /// specified location.
        /// </summary>
        private void DrawGroupBox(GroupBox grp, Point p, Graphics g)
        {
            // Control's BackColor
            g.FillRectangle(new SolidBrush(grp.BackColor),
                p.X,
                p.Y,
                grp.Width,
                grp.Height);

            // Draw a rectangle leaving space for the heading
            int iFontHeight = (int)g.MeasureString(grp.Text, grp.Font).Height;
            Point[] pt = new Point[6];
            pt[0].X = p.X + 10;
            pt[0].Y = p.Y + (iFontHeight / 2);
            pt[1].X = p.X;
            pt[1].Y = p.Y + (iFontHeight / 2);
            pt[2].X = p.X;
            pt[2].Y = p.Y + grp.Height;
            pt[3].X = p.X + grp.Width;
            pt[3].Y = p.Y + grp.Height;
            pt[4].X = p.X + grp.Width;
            pt[4].Y = p.Y + (iFontHeight / 2);
            pt[5].X = p.X + (int)g.MeasureString(grp.Text, grp.Font).Width + 10 + 2;
            if (pt[5].X < p.X)
                pt[5].X = p.X;
            pt[5].Y = p.Y + (iFontHeight / 2);
            g.DrawLines(new Pen(new SolidBrush(grp.ForeColor), 1), pt);

            // GroupBox's text
            g.DrawString(grp.Text,
                grp.Font,
                new SolidBrush(grp.ForeColor),
                p.X + 10 + 2,
                p.Y,
                new StringFormat());
        }

        /// <summary>
        /// Draws the specified TextBox on the specified graphics object at the
        /// specified location.
        /// </summary>
        private void DrawTextBox(TextBox txt, Point p, Graphics g)
        {
            int iBorder = 2;

            // Draw the TextBox border
            ControlPaint.DrawBorder(g,
                new Rectangle(p.X, p.Y, txt.Width, txt.Height),
                SystemColors.Control,
                iBorder,
                ButtonBorderStyle.Inset,
                SystemColors.Control,
                iBorder,
                ButtonBorderStyle.Inset,
                SystemColors.Control,
                iBorder,
                ButtonBorderStyle.Inset,
                SystemColors.Control,
                iBorder,
                ButtonBorderStyle.Inset);

            // Control's BackColor
            g.FillRectangle(new SolidBrush(txt.BackColor),
                p.X + iBorder,
                p.Y + iBorder,
                txt.Width - 4,
                txt.Height - 5);

            // TextBox's text left justified & centered vertically
            g.DrawString(txt.Text,
                txt.Font,
                new SolidBrush(txt.ForeColor),
                p.X + iBorder,
                p.Y + (txt.Height / 2) - (g.MeasureString(txt.Text, txt.Font).Height / 2));
        }

        /// <summary>
        /// Draws the specified ComboBox on the specified graphics object at the
        /// specified location.
        /// </summary>
        /// <remarks>
        /// The size of the Combo button itself is hard-coded here and was determined by
        /// using a graphics program.
        /// </remarks>
        private void DrawComboBox(ComboBox cbo, Point p, Graphics g)
        {
            int iBorder = 2;

            // Draw the TextBox border
            ControlPaint.DrawBorder(g,
                new Rectangle(p.X, p.Y, cbo.Width, cbo.Height),
                SystemColors.Control,
                iBorder,
                ButtonBorderStyle.Inset,
                SystemColors.Control,
                iBorder,
                ButtonBorderStyle.Inset,
                SystemColors.Control,
                iBorder,
                ButtonBorderStyle.Inset,
                SystemColors.Control,
                iBorder,
                ButtonBorderStyle.Inset);

            // Control's BackColor
            g.FillRectangle(new SolidBrush(cbo.BackColor),
                p.X + iBorder,
                p.Y + iBorder,
                cbo.Width - 4,
                cbo.Height - 4);

            // ComboBox's text left justified & centered vertically
            g.DrawString(cbo.Text,
                cbo.Font,
                new SolidBrush(cbo.ForeColor),
                p.X + iBorder,
                p.Y + (cbo.Height / 2) - (g.MeasureString(cbo.Text, cbo.Font).Height / 2));

            // ComboBox's drop down arrow button thingy
            ControlPaint.DrawComboButton(g,
                p.X + cbo.Width - 16 - iBorder,
                p.Y + (cbo.Height / 2) - (16 / 2),
                16,
                17,
                ButtonState.Normal);
        }

        /// <summary>
        /// Draws the specified Label on the specified graphics object at the
        /// specified location.
        /// </summary>
        private void DrawLabel(Label lbl, Point p, Graphics g)
        {
            // Control's BackColor
            g.FillRectangle(new SolidBrush(lbl.BackColor),
                p.X,
                p.Y,
                lbl.Width,
                lbl.Height);

            // Label's text centered on both axis
            g.DrawString(
                lbl.Text,
                lbl.Font,
                new SolidBrush(lbl.ForeColor),
                p.X + (lbl.Width / 2) - (g.MeasureString(lbl.Text, lbl.Font).Width / 2),
                p.Y + (lbl.Height / 2) - (g.MeasureString(lbl.Text, lbl.Font).Height / 2));
        }

        /// <summary>
        /// Draws the specified Button on the specified graphics object at the
        /// specified location.
        /// </summary>
        private void DrawButton(Button btn, Point p, Graphics g)
        {
            ControlPaint.DrawButton(g, p.X, p.Y, btn.Width, btn.Height, ButtonState.Normal);

            // Button's text centered on both axis
            g.DrawString(
                btn.Text,
                btn.Font,
                new SolidBrush(btn.ForeColor),
                p.X + (btn.Width / 2) - (g.MeasureString(btn.Text, btn.Font).Width / 2),
                p.Y + (btn.Height / 2) - (g.MeasureString(btn.Text, btn.Font).Height / 2));
        }

        /// <summary>
        /// Draws the specified CheckBox on the specified graphics object at the
        /// specified location.
        /// </summary>
        /// <remarks>
        /// The size of the CheckBox itself is hard-coded here and was determined by
        /// using a graphics program.
        /// </remarks>
        private void DrawCheckBox(CheckBox chk, Point p, Graphics g)
        {
            // Set up the size of a CheckBox
            Rectangle rCheckBox = new Rectangle(p.X, p.Y, 13, 13);

            ControlPaint.DrawCheckBox(g, p.X,
                p.Y + (chk.Height / 2) - (rCheckBox.Height / 2),
                rCheckBox.Width,
                rCheckBox.Height,
                (chk.Checked ? ButtonState.Checked : ButtonState.Normal));

            // CheckBox's text left justified & centered vertically
            g.DrawString(chk.Text,
                chk.Font,
                new SolidBrush(chk.ForeColor),
                rCheckBox.Right + 1,
                p.Y + (chk.Height / 2) - (g.MeasureString(chk.Text, chk.Font).Height / 2));
        }

        /// <summary>
        /// Draws the specified RadioButton on the specified graphics object at the
        /// specified location.
        /// </summary>
        /// <remarks>
        /// The size of the RadioButton itself is hard-coded here and was determined by
        /// using a graphics program.
        /// </remarks>
        private void DrawRadioButton(RadioButton rdo, Point p, Graphics g)
        {
            // Setup the size of a RadioButton
            Rectangle rRadioButton = new Rectangle(p.X, p.Y, 12, 12);

            ControlPaint.DrawRadioButton(g, p.X,
                p.Y + (rdo.Height / 2) - (rRadioButton.Height / 2),
                rRadioButton.Width,
                rRadioButton.Height,
                (rdo.Checked ? ButtonState.Checked : ButtonState.Normal));

            // RadioButton's text left justified & centered vertically
            g.DrawString(rdo.Text,
                rdo.Font,
                new SolidBrush(rdo.ForeColor),
                rRadioButton.Right + 1,
                p.Y + (rdo.Height / 2) - (g.MeasureString(rdo.Text, rdo.Font).Height / 2));
        }

        #endregion

        #region Event Handlers

        private void PrintDocument_BeginPrint(object sender, PrintEventArgs e)
        {
            _iCurrentPageIndex = 0;
        }
        /// <summary>
        /// //../////
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PrintDocument_PrintPage(object sender, PrintPageEventArgs e)
        {
            // Set the page margins
            Rectangle rPageMargins = new Rectangle(e.MarginBounds.Location, e.MarginBounds.Size);

            // Generate the offset origins for the printing window
            Point[] ptOffsets = GeneratePrintingOffsets(rPageMargins);

            // Make sure nothing gets printed in the margins
            e.Graphics.SetClip(rPageMargins);

            // Draw the rest of the Form using the calculated offsets
            Point ptOffset = new Point(-ptOffsets[_iCurrentPageIndex].X, -ptOffsets[_iCurrentPageIndex].Y);
            ptOffset.Offset(rPageMargins.X, rPageMargins.Y);
            DrawForm(e.Graphics, ptOffset);

            // Determine if there are more pages
            e.HasMorePages = (_iCurrentPageIndex < ptOffsets.Length - 1);

            _iCurrentPageIndex++;
        }

        #endregion
    }
}

﻿namespace DarziApp
{
    partial class CheckOrder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.lbl_Name = new System.Windows.Forms.Label();
            this.lbl_Phone = new System.Windows.Forms.Label();
            this.lbl_Size = new System.Windows.Forms.Label();
            this.lbl_Desc = new System.Windows.Forms.Label();
            this.lbl_Suit = new System.Windows.Forms.Label();
            this.lbl_payment = new System.Windows.Forms.Label();
            this.lbl_adv = new System.Windows.Forms.Label();
            this.lbl_date = new System.Windows.Forms.Label();
            this.lbl_ddate = new System.Windows.Forms.Label();
            this.lbl_orderNumber = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.btn_confirm = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(121, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(238, 39);
            this.label1.TabIndex = 0;
            this.label1.Text = "Confirm Order";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // lbl_Name
            // 
            this.lbl_Name.AutoSize = true;
            this.lbl_Name.Location = new System.Drawing.Point(173, 114);
            this.lbl_Name.Name = "lbl_Name";
            this.lbl_Name.Size = new System.Drawing.Size(21, 13);
            this.lbl_Name.TabIndex = 1;
            this.lbl_Name.Text = "AS";
            // 
            // lbl_Phone
            // 
            this.lbl_Phone.AutoSize = true;
            this.lbl_Phone.Location = new System.Drawing.Point(173, 136);
            this.lbl_Phone.Name = "lbl_Phone";
            this.lbl_Phone.Size = new System.Drawing.Size(21, 13);
            this.lbl_Phone.TabIndex = 1;
            this.lbl_Phone.Text = "AS";
            // 
            // lbl_Size
            // 
            this.lbl_Size.AutoSize = true;
            this.lbl_Size.Location = new System.Drawing.Point(319, 114);
            this.lbl_Size.Name = "lbl_Size";
            this.lbl_Size.Size = new System.Drawing.Size(21, 13);
            this.lbl_Size.TabIndex = 1;
            this.lbl_Size.Text = "AS";
            // 
            // lbl_Desc
            // 
            this.lbl_Desc.AutoSize = true;
            this.lbl_Desc.Location = new System.Drawing.Point(445, 114);
            this.lbl_Desc.Name = "lbl_Desc";
            this.lbl_Desc.Size = new System.Drawing.Size(21, 13);
            this.lbl_Desc.TabIndex = 1;
            this.lbl_Desc.Text = "AS";
            // 
            // lbl_Suit
            // 
            this.lbl_Suit.AutoSize = true;
            this.lbl_Suit.Location = new System.Drawing.Point(173, 195);
            this.lbl_Suit.Name = "lbl_Suit";
            this.lbl_Suit.Size = new System.Drawing.Size(21, 13);
            this.lbl_Suit.TabIndex = 1;
            this.lbl_Suit.Text = "AS";
            // 
            // lbl_payment
            // 
            this.lbl_payment.AutoSize = true;
            this.lbl_payment.Location = new System.Drawing.Point(173, 217);
            this.lbl_payment.Name = "lbl_payment";
            this.lbl_payment.Size = new System.Drawing.Size(21, 13);
            this.lbl_payment.TabIndex = 1;
            this.lbl_payment.Text = "AS";
            // 
            // lbl_adv
            // 
            this.lbl_adv.AutoSize = true;
            this.lbl_adv.Location = new System.Drawing.Point(173, 239);
            this.lbl_adv.Name = "lbl_adv";
            this.lbl_adv.Size = new System.Drawing.Size(21, 13);
            this.lbl_adv.TabIndex = 1;
            this.lbl_adv.Text = "AS";
            // 
            // lbl_date
            // 
            this.lbl_date.AutoSize = true;
            this.lbl_date.Location = new System.Drawing.Point(173, 262);
            this.lbl_date.Name = "lbl_date";
            this.lbl_date.Size = new System.Drawing.Size(21, 13);
            this.lbl_date.TabIndex = 1;
            this.lbl_date.Text = "AS";
            // 
            // lbl_ddate
            // 
            this.lbl_ddate.AutoSize = true;
            this.lbl_ddate.Location = new System.Drawing.Point(173, 284);
            this.lbl_ddate.Name = "lbl_ddate";
            this.lbl_ddate.Size = new System.Drawing.Size(21, 13);
            this.lbl_ddate.TabIndex = 1;
            this.lbl_ddate.Text = "AS";
            // 
            // lbl_orderNumber
            // 
            this.lbl_orderNumber.AutoSize = true;
            this.lbl_orderNumber.Location = new System.Drawing.Point(454, 9);
            this.lbl_orderNumber.Name = "lbl_orderNumber";
            this.lbl_orderNumber.Size = new System.Drawing.Size(21, 13);
            this.lbl_orderNumber.TabIndex = 1;
            this.lbl_orderNumber.Text = "AS";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 114);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Name:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 217);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Total Payment:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 136);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Phone:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 239);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(97, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Advance Payment:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(319, 92);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(30, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "Size:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 262);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(62, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Order Date:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(445, 92);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(63, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "Description:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 284);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(56, 13);
            this.label9.TabIndex = 1;
            this.label9.Text = "Due Date:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(12, 195);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(70, 13);
            this.label10.TabIndex = 1;
            this.label10.Text = "Suit Quantity:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(378, 9);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(76, 13);
            this.label11.TabIndex = 1;
            this.label11.Text = "Order Number:";
            // 
            // btn_confirm
            // 
            this.btn_confirm.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btn_confirm.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_confirm.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_confirm.Location = new System.Drawing.Point(225, 364);
            this.btn_confirm.Name = "btn_confirm";
            this.btn_confirm.Size = new System.Drawing.Size(124, 23);
            this.btn_confirm.TabIndex = 2;
            this.btn_confirm.TabStop = false;
            this.btn_confirm.Text = "Confirm Order";
            this.btn_confirm.UseVisualStyleBackColor = false;
            this.btn_confirm.Click += new System.EventHandler(this.btn_confirm_Click);
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::DarziApp.Properties.Resources.logo;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel1.Location = new System.Drawing.Point(9, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(100, 67);
            this.panel1.TabIndex = 3;
            // 
            // CheckOrder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(581, 394);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btn_confirm);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.lbl_orderNumber);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.lbl_Suit);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.lbl_ddate);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.lbl_Desc);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.lbl_date);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lbl_Size);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lbl_adv);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lbl_Phone);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lbl_payment);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lbl_Name);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "CheckOrder";
            this.ShowInTaskbar = false;
            this.Text = "Check Order";
            this.Activated += new System.EventHandler(this.CheckOrder_Activated);
            this.Load += new System.EventHandler(this.CheckOrder_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbl_Name;
        private System.Windows.Forms.Label lbl_Phone;
        private System.Windows.Forms.Label lbl_Size;
        private System.Windows.Forms.Label lbl_Desc;
        private System.Windows.Forms.Label lbl_Suit;
        private System.Windows.Forms.Label lbl_payment;
        private System.Windows.Forms.Label lbl_adv;
        private System.Windows.Forms.Label lbl_date;
        private System.Windows.Forms.Label lbl_ddate;
        private System.Windows.Forms.Label lbl_orderNumber;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btn_confirm;
        private System.Windows.Forms.Panel panel1;
    }
}
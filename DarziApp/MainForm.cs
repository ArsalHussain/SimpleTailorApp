﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace DarziApp
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'database1DataSet.Orders' table. You can move, or remove it, as needed.
            this.ordersTableAdapter.Fill(this.database1DataSet.Orders);
            // TODO: This line of code loads data into the 'mainDatabaseDataSet.Orders' table. You can move, or remove it, as needed.
            //this.ordersTableAdapter.Fill(this.mainDatabaseDataSet.Orders);
            // INSERT INTO [dbo].[Orders] ( [ordernumber], [phonenumber], [suitquatity], [totalpayment], [advancepayment], [size], [description], [orderdate], [duedate], [name]) VALUES ( N'safdfds', N'safdfds', N'safdfds', N'safdfds', N'safdfds', N'safdfds', N'safdfds', N'safdfds', N'safdfds', N'safdfdssafdfds')
            //MessageBox.Show(StaticValuesHolder.CONNECTIONQUERY);

            if (DateTime.Now > DateTime.Parse("24-Dec-16"))
            {
                if (!File.Exists(AppDomain.CurrentDomain.BaseDirectory + "/APPLICENCE12345.txt"))
                {
                    MessageBox.Show("Sorry The Trial Period Is Over, Please Buy This Product To Continue Use\n آپ کا چیکنگ ٹائم ختم ہوگیا ہے، مستقل استعمال کے لیئے اس کو خریدیئے۔ شکریہ", "Trial Expired" , MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Close();
                }
            }
        }



        private void toolStripSplitButton1_ButtonClick(object sender, EventArgs e)
        {
            newOrderDiag.Visible = true;

            viewAllTrans.Visible = false;
            viewClients.Visible = false;
        }

        private void btn_new_Click(object sender, EventArgs e)
        {
            this.ordersTableAdapter.Fill(this.database1DataSet.Orders);
            newOrderDiag.Visible = true;

            viewAllTrans.Visible = false;
            viewClients.Visible = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            String Size = "";
            String Description = "";
            
            foreach (var pb in newOrderPanel.Controls.OfType<NumericUpDown>())
            {
               Size += pb.Name.Split('_')[1] +":"+ pb.Value + "/ ";
            }

            foreach (var pb in newOrderPanel.Controls.OfType<CheckBox>())
            {
                Description += pb.Checked ? pb.Text + "/ " : "";
            }
            StaticValuesHolder.SIZE = Size;
            StaticValuesHolder.DESCRIPTION = Description;

          //  (new CheckOrder()).ShowDialog();
             (new ClientInfo()).ShowDialog();
        }

        private void label10_Click(object sender, EventArgs e)
        {
            viewAllTrans.Visible = false;
            viewClients.Visible = false;
            newOrderDiag.Visible = false;
        }

        private void btn_search_Click(object sender, EventArgs e)
        {
            if (inp_numberSearch.MaskFull)
            {
                //MessageBox.Show(inp_numberSearch.Text);
                QuerifytoFillGrid(String.Format("SELECT Id, ordernumber, phonenumber, suitquatity, totalpayment, advancepayment, size, description, orderdate, duedate, name FROM dbo.Orders where phonenumber = '{0}'", inp_numberSearch.Text));
            }
            else
            {
                MessageBox.Show("Please Enter Complete Contact Number.");
            }
        }

        private void QuerifytoFillGrid(string query)
        {
            SqlConnection con = new SqlConnection(StaticValuesHolder.CONNECTIONQUERY);

            try
            {
                con.Close();
                con.Open();
                SqlDataAdapter dataAdapter = new SqlDataAdapter(query, con);
                SqlCommandBuilder commandBuilder = new SqlCommandBuilder(dataAdapter);
                DataSet ds = new DataSet();
                dataAdapter.Fill(ds);
                GridViewSearch.ReadOnly = true;
                GridViewSearch.DataSource = ds.Tables[0];
            }
            finally
            {
                con.Close();
            }
        }

        private void viewClientsX_Click(object sender, EventArgs e)
        {
            viewAllTrans.Visible = false;
            viewClients.Visible = false;
            newOrderDiag.Visible = false;
        }

        private void btn_view_Click(object sender, EventArgs e)
        {
            this.ordersTableAdapter.Fill(this.database1DataSet.Orders);
            viewClients.Visible = true;

            viewAllTrans.Visible = false;
            newOrderDiag.Visible = false;
        }
        
        private void btn_history_Click(object sender, EventArgs e)
        {

            this.ordersTableAdapter.Fill(this.database1DataSet.Orders);
            viewAllTrans.Visible = true;

            viewClients.Visible = false;
            newOrderDiag.Visible = false;
        }

        private void closeOrders_Click(object sender, EventArgs e)
        {
            viewAllTrans.Visible = false;
            viewClients.Visible = false;
            newOrderDiag.Visible = false;
        }

       

        private void btn_home_Click(object sender, EventArgs e)
        {
            //String input = "B:6\nL1:9\nD:12\nW:6\nC:9\nA:15\nS:12\nL:3";

            //MessageBox.Show(Between(input,"B:","\n"));

            viewAllTrans.Visible = false;
            viewClients.Visible = false;
            newOrderDiag.Visible = false;
        }


        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                PrintForm pf = new PrintForm(this);
                pf.Print();
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void GridViewSearch_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            //This Line Will Give Column Value -->   dataGridView1.SelectedRows[0].Cells[2].Value.ToString()
            //  Name Address Phone Amount Status

            if (e.RowIndex < 0)
                return;

            //string desc, size;
            //desc = StaticValuesHolder.DESCRIPTION;
            //desc.Replace("/ ", "\n");

            //size = StaticValuesHolder.SIZE;
            //size.Replace("/ ", "\n");

            StaticValuesHolder.NAME = GridViewSearch.SelectedRows[0].Cells[0].Value.ToString();
            StaticValuesHolder.PHONE = GridViewSearch.SelectedRows[0].Cells[1].Value.ToString();
            StaticValuesHolder.SUITQUANTITY = GridViewSearch.SelectedRows[0].Cells[2].Value.ToString();
            StaticValuesHolder.TOTALPAYMENT = GridViewSearch.SelectedRows[0].Cells[3].Value.ToString();
            StaticValuesHolder.ADVANCEPAYMENT =GridViewSearch.SelectedRows[0].Cells[4].Value.ToString();
            StaticValuesHolder.SIZE = (GridViewSearch.SelectedRows[0].Cells[5].Value.ToString()).Replace("/ ", "\n");
            StaticValuesHolder.DESCRIPTION = (GridViewSearch.SelectedRows[0].Cells[6].Value.ToString()).Replace("/ ", "\n");
            StaticValuesHolder.ORDERDATE = GridViewSearch.SelectedRows[0].Cells[7].Value.ToString();
            StaticValuesHolder.DUEDATE = GridViewSearch.SelectedRows[0].Cells[8].Value.ToString();
            StaticValuesHolder.ORDERNUMBER = GridViewSearch.SelectedRows[0].Cells[9].Value.ToString();

            //Clipboard.SetText(StaticValuesHolder.SIZE);
            /// TO DO: asa
            
            DialogResult answer = new MessageBoxCustom(GridViewSearch.SelectedRows[0].Cells[10].Value.ToString()).ShowDialog();
            if ( answer == System.Windows.Forms.DialogResult.OK)
            {
                #region
                this.ordersTableAdapter.Fill(this.database1DataSet.Orders);
                newOrderDiag.Visible = true;

                viewAllTrans.Visible = false;
                viewClients.Visible = false;

                //string[] array =  (StaticValuesHolder.SIZE).Split('\n');
                //string output = Regex.Replace(StaticValuesHolder.SIZE, "[^0-9\n]+", string.Empty);
                //MessageBox.Show(output);
                //array = output.Split('\n');

                StaticValuesHolder.SIZE = GridViewSearch.SelectedRows[0].Cells[5].Value.ToString();

                String[] sizes = StaticValuesHolder.SIZE.Split(new String[] { "/ " }, StringSplitOptions.None);
                //MessageBox.Show(sizes[0].Replace("B:", "") + "-");


                inp_B.Value = Int32.Parse(sizes[0].Replace("B:", ""));
                inp_L1.Value = Int32.Parse(sizes[1].Replace("L1:", ""));
                inp_D.Value = Int32.Parse(sizes[2].Replace("D:", ""));
                inp_W.Value = Int32.Parse(sizes[3].Replace("W:", ""));
                inp_C.Value = Int32.Parse(sizes[4].Replace("C:", ""));
                inp_A.Value = Int32.Parse(sizes[5].Replace("A:", ""));
                inp_S.Value = Int32.Parse(sizes[6].Replace("S:", ""));
                inp_L.Value = Int32.Parse(sizes[7].Replace("L:", ""));

                #region
                //   "B:8\nL1:7\nD:6\nW:5\nC:4\nA:3\nS:2\nL:1\n"

                //// Label B Value:
                //int pFrom = StaticValuesHolder.SIZE.IndexOf("B:") + "B:".Length;
                //int pTo = StaticValuesHolder.SIZE.LastIndexOf("\n");
                //String result = StaticValuesHolder.SIZE.Substring(pFrom, pTo - pFrom);
                //MessageBox.Show(Regex.Replace(result, "[^0-9]+", string.Empty).ToString());
                //inp_B.Value = Int32.Parse(Between(StaticValuesHolder.SIZE, @"B:", @"\n"));


                //// Label L1 Value:
                //pFrom = StaticValuesHolder.SIZE.IndexOf("L1:") + "L1:".Length;
                //pTo = StaticValuesHolder.SIZE.LastIndexOf("\n");
                //result = StaticValuesHolder.SIZE.Substring(pFrom, pTo - pFrom);
                //MessageBox.Show(Between(StaticValuesHolder.SIZE, "\nL1:", "\n"));
                //inp_L1.Value = Int32.Parse(Between(StaticValuesHolder.SIZE, @"\\nL1:", @"\n"));

                //// Label D Value:
                //pFrom = StaticValuesHolder.SIZE.IndexOf("D:") + "D:".Length;
                //pTo = StaticValuesHolder.SIZE.LastIndexOf("\n");
                //result = StaticValuesHolder.SIZE.Substring(pFrom, pTo - pFrom);
                //inp_D.Value = Int32.Parse(Between(StaticValuesHolder.SIZE, @"D:", @"\n"));

                //// Label W Value:
                
                //pFrom = StaticValuesHolder.SIZE.IndexOf("W:") + "W:".Length;
                //pTo = StaticValuesHolder.SIZE.LastIndexOf("\n");
                //result = StaticValuesHolder.SIZE.Substring(pFrom, pTo - pFrom);
                //inp_W.Value = Int32.Parse(Between(StaticValuesHolder.SIZE, @"W:", @"\n"));
                
                //// Label C Value:
                //pFrom = StaticValuesHolder.SIZE.IndexOf("C:") + "C:".Length;
                //pTo = StaticValuesHolder.SIZE.LastIndexOf("\n");
                //result = StaticValuesHolder.SIZE.Substring(pFrom, pTo - pFrom);
                //inp_C.Value = Int32.Parse(Between(StaticValuesHolder.SIZE, @"C:", @"\n"));

                //// Label A Value:
                //pFrom = StaticValuesHolder.SIZE.IndexOf("A:") + "A:".Length;
                //pTo = StaticValuesHolder.SIZE.LastIndexOf("\n");
                //result = StaticValuesHolder.SIZE.Substring(pFrom, pTo - pFrom);
                //inp_A.Value = Int32.Parse(Between(StaticValuesHolder.SIZE, @"A:", @"\n"));

                //// Label S Value:
                //pFrom = StaticValuesHolder.SIZE.IndexOf("S:") + "S:".Length;
                //pTo = StaticValuesHolder.SIZE.LastIndexOf("\n");
                //result = StaticValuesHolder.SIZE.Substring(pFrom, pTo - pFrom);
                //inp_S.Value = Int32.Parse(Between(StaticValuesHolder.SIZE, @"S:", @"\n"));

                //// Label L Value:
                //pFrom = StaticValuesHolder.SIZE.IndexOf("L:") + "L:".Length;
                //pTo = StaticValuesHolder.SIZE.LastIndexOf("\n");
                //result = StaticValuesHolder.SIZE.Substring(pFrom, pTo - pFrom);
                //inp_L.Value = Int32.Parse(Between(StaticValuesHolder.SIZE, @"L:", @"\n"));
                
                //MessageBox.Show(result);

                





                //int i =1;
                //foreach (var pb in newOrderPanel.Controls.OfType<NumericUpDown>())
                //{
                //  //  Size += pb.Name.Split('_')[1] + ":" + pb.Value + "/ ";
                //    int number = 10;

                //    //bool result = Int32.TryParse(array[i], out number);
                //    //pb.Value = number;
                //    //i++;

                //}

                #endregion


                #endregion
            }
            else if (answer == System.Windows.Forms.DialogResult.Yes)
            {
                this.ordersTableAdapter.Fill(this.database1DataSet.Orders);
            }
        }

        private void inp_L_Enter(object sender, EventArgs e)
        {
            ((NumericUpDown)sender).Select(0, ((NumericUpDown)sender).Text.Length);
        }

        private void inp_B_MouseDown(object sender, MouseEventArgs e)
        {
            ((NumericUpDown)sender).Select(0, ((NumericUpDown)sender).Text.Length);
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.ordersTableAdapter.Fill(this.database1DataSet.Orders);
            newOrderDiag.Visible = true;

            viewAllTrans.Visible = false;
            viewClients.Visible = false;
        }

        private void exitToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void aboutSoftwareToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("This Software Is Developed By Us, Feel Free To e-Mail Us At:\narsalhussain@outlook.com","About Developers", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}

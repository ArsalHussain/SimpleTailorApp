﻿namespace DarziApp
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.helpContentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.aboutSoftwareToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.navigationpanel = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripSplitButton1 = new System.Windows.Forms.ToolStripSplitButton();
            this.bottomstrip = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.mainmenustrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.menupanel = new System.Windows.Forms.Panel();
            this.btn_new = new System.Windows.Forms.Button();
            this.btn_view = new System.Windows.Forms.Button();
            this.btn_history = new System.Windows.Forms.Button();
            this.btn_home = new System.Windows.Forms.Button();
            this.maincontainerpanel = new System.Windows.Forms.Panel();
            this.newOrderDiag = new System.Windows.Forms.Panel();
            this.newOrderPanel = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.btn_check = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.inp_AsteenBukram = new System.Windows.Forms.CheckBox();
            this.inp_pocketsada = new System.Windows.Forms.CheckBox();
            this.inp_kurtaKallionWala = new System.Windows.Forms.CheckBox();
            this.inp_B = new System.Windows.Forms.NumericUpDown();
            this.inp_fullsingle = new System.Windows.Forms.CheckBox();
            this.inp_jaamSilaye = new System.Windows.Forms.CheckBox();
            this.inp_singleDhaga = new System.Windows.Forms.CheckBox();
            this.inp_L1 = new System.Windows.Forms.NumericUpDown();
            this.inp_awaamiKurta = new System.Windows.Forms.CheckBox();
            this.inp_fullbarabar = new System.Windows.Forms.CheckBox();
            this.inp_D = new System.Windows.Forms.NumericUpDown();
            this.inp_Asteen = new System.Windows.Forms.CheckBox();
            this.inp_W = new System.Windows.Forms.NumericUpDown();
            this.inp_fullDouble = new System.Windows.Forms.CheckBox();
            this.inp_DubKanta = new System.Windows.Forms.CheckBox();
            this.inp_C = new System.Windows.Forms.NumericUpDown();
            this.inp_gumSilaye = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.inp_readyMade = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.inp_A = new System.Windows.Forms.NumericUpDown();
            this.inp_kaf = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.inp_Romaali = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.inp_S = new System.Windows.Forms.NumericUpDown();
            this.inp_shairwaniCollar = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.inp_chamakDhaga = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.inp_L = new System.Windows.Forms.NumericUpDown();
            this.inp_collarNookdar = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.inp_doubleDhaga = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.viewClients = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.GridViewSearch = new System.Windows.Forms.DataGridView();
            this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.phonenumberDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.suitquatityDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totalpaymentDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.advancepaymentDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sizeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descriptionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.orderdateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.duedateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ordernumberDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ordersBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.database1DataSet = new DarziApp.Database1DataSet();
            this.inp_numberSearch = new System.Windows.Forms.MaskedTextBox();
            this.viewClientsX = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.btn_search = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.viewAllTrans = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.closeOrders = new System.Windows.Forms.Label();
            this.GridViewAll = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ordernumberDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.phonenumberDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.suitquatityDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totalpaymentDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.advancepaymentDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sizeDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descriptionDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.orderdateDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.duedateDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label17 = new System.Windows.Forms.Label();
            this.ordersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ordersTableAdapter = new DarziApp.Database1DataSetTableAdapters.OrdersTableAdapter();
            this.navigationpanel.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.bottomstrip.SuspendLayout();
            this.mainmenustrip.SuspendLayout();
            this.menupanel.SuspendLayout();
            this.maincontainerpanel.SuspendLayout();
            this.newOrderDiag.SuspendLayout();
            this.newOrderPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.inp_B)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inp_L1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inp_D)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inp_W)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inp_C)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inp_A)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inp_S)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inp_L)).BeginInit();
            this.viewClients.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridViewSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ordersBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.database1DataSet)).BeginInit();
            this.viewAllTrans.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridViewAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ordersBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // helpContentToolStripMenuItem
            // 
            this.helpContentToolStripMenuItem.Name = "helpContentToolStripMenuItem";
            this.helpContentToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.helpContentToolStripMenuItem.Text = "Help Content";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(153, 6);
            // 
            // aboutSoftwareToolStripMenuItem
            // 
            this.aboutSoftwareToolStripMenuItem.Name = "aboutSoftwareToolStripMenuItem";
            this.aboutSoftwareToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.aboutSoftwareToolStripMenuItem.Text = "About Software";
            this.aboutSoftwareToolStripMenuItem.Click += new System.EventHandler(this.aboutSoftwareToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpContentToolStripMenuItem,
            this.toolStripSeparator3,
            this.aboutSoftwareToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // navigationpanel
            // 
            this.navigationpanel.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.navigationpanel.Controls.Add(this.panel1);
            this.navigationpanel.Controls.Add(this.statusStrip1);
            this.navigationpanel.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.navigationpanel.Location = new System.Drawing.Point(0, 27);
            this.navigationpanel.Name = "navigationpanel";
            this.navigationpanel.Size = new System.Drawing.Size(784, 33);
            this.navigationpanel.TabIndex = 7;
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.BackgroundImage = global::DarziApp.Properties.Resources.main;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Location = new System.Drawing.Point(164, 31);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(620, 431);
            this.panel1.TabIndex = 9;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSplitButton1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 0);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(784, 33);
            this.statusStrip1.SizingGrip = false;
            this.statusStrip1.TabIndex = 0;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripSplitButton1
            // 
            this.toolStripSplitButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSplitButton1.Image")));
            this.toolStripSplitButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSplitButton1.Name = "toolStripSplitButton1";
            this.toolStripSplitButton1.Size = new System.Drawing.Size(96, 31);
            this.toolStripSplitButton1.Text = "New Order";
            this.toolStripSplitButton1.ButtonClick += new System.EventHandler(this.toolStripSplitButton1_ButtonClick);
            // 
            // bottomstrip
            // 
            this.bottomstrip.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.bottomstrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripStatusLabel2});
            this.bottomstrip.Location = new System.Drawing.Point(0, 489);
            this.bottomstrip.Name = "bottomstrip";
            this.bottomstrip.Size = new System.Drawing.Size(784, 22);
            this.bottomstrip.SizingGrip = false;
            this.bottomstrip.TabIndex = 4;
            this.bottomstrip.Text = "bottomstrip";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(238, 17);
            this.toolStripStatusLabel1.Text = "Contact: 0321-2763391 or 0315-0294191";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(59, 17);
            this.toolStripStatusLabel2.Text = "I.D Tailors";
            // 
            // mainmenustrip
            // 
            this.mainmenustrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.mainmenustrip.Location = new System.Drawing.Point(0, 0);
            this.mainmenustrip.Name = "mainmenustrip";
            this.mainmenustrip.Size = new System.Drawing.Size(784, 24);
            this.mainmenustrip.TabIndex = 5;
            this.mainmenustrip.Text = "mainmenustrip";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem,
            this.toolStripSeparator1,
            this.exitToolStripMenuItem1});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.exitToolStripMenuItem.Text = "New";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(140, 6);
            // 
            // exitToolStripMenuItem1
            // 
            this.exitToolStripMenuItem1.Name = "exitToolStripMenuItem1";
            this.exitToolStripMenuItem1.ShortcutKeyDisplayString = "Ctrl + W";
            this.exitToolStripMenuItem1.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.W)));
            this.exitToolStripMenuItem1.Size = new System.Drawing.Size(143, 22);
            this.exitToolStripMenuItem1.Text = "Exit";
            this.exitToolStripMenuItem1.Click += new System.EventHandler(this.exitToolStripMenuItem1_Click);
            // 
            // menupanel
            // 
            this.menupanel.BackColor = System.Drawing.Color.Transparent;
            this.menupanel.Controls.Add(this.btn_new);
            this.menupanel.Controls.Add(this.btn_view);
            this.menupanel.Controls.Add(this.btn_history);
            this.menupanel.Controls.Add(this.btn_home);
            this.menupanel.Location = new System.Drawing.Point(0, 58);
            this.menupanel.Name = "menupanel";
            this.menupanel.Size = new System.Drawing.Size(165, 431);
            this.menupanel.TabIndex = 6;
            // 
            // btn_new
            // 
            this.btn_new.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btn_new.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_new.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_new.Location = new System.Drawing.Point(10, 157);
            this.btn_new.Name = "btn_new";
            this.btn_new.Size = new System.Drawing.Size(146, 23);
            this.btn_new.TabIndex = 1;
            this.btn_new.TabStop = false;
            this.btn_new.Text = "Take Measurement";
            this.btn_new.UseVisualStyleBackColor = false;
            this.btn_new.Click += new System.EventHandler(this.btn_new_Click);
            // 
            // btn_view
            // 
            this.btn_view.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btn_view.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_view.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_view.Location = new System.Drawing.Point(10, 186);
            this.btn_view.Name = "btn_view";
            this.btn_view.Size = new System.Drawing.Size(146, 23);
            this.btn_view.TabIndex = 1;
            this.btn_view.TabStop = false;
            this.btn_view.Text = "View Client";
            this.btn_view.UseVisualStyleBackColor = false;
            this.btn_view.Click += new System.EventHandler(this.btn_view_Click);
            // 
            // btn_history
            // 
            this.btn_history.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btn_history.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_history.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_history.Location = new System.Drawing.Point(10, 215);
            this.btn_history.Name = "btn_history";
            this.btn_history.Size = new System.Drawing.Size(146, 23);
            this.btn_history.TabIndex = 1;
            this.btn_history.TabStop = false;
            this.btn_history.Text = "History";
            this.btn_history.UseVisualStyleBackColor = false;
            this.btn_history.Click += new System.EventHandler(this.btn_history_Click);
            // 
            // btn_home
            // 
            this.btn_home.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btn_home.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_home.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_home.Location = new System.Drawing.Point(10, 128);
            this.btn_home.Name = "btn_home";
            this.btn_home.Size = new System.Drawing.Size(146, 23);
            this.btn_home.TabIndex = 1;
            this.btn_home.TabStop = false;
            this.btn_home.Text = "Home";
            this.btn_home.UseVisualStyleBackColor = false;
            this.btn_home.Click += new System.EventHandler(this.btn_home_Click);
            // 
            // maincontainerpanel
            // 
            this.maincontainerpanel.AutoScroll = true;
            this.maincontainerpanel.BackColor = System.Drawing.Color.Transparent;
            this.maincontainerpanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.maincontainerpanel.Controls.Add(this.newOrderDiag);
            this.maincontainerpanel.Controls.Add(this.viewClients);
            this.maincontainerpanel.Controls.Add(this.viewAllTrans);
            this.maincontainerpanel.Location = new System.Drawing.Point(164, 58);
            this.maincontainerpanel.Name = "maincontainerpanel";
            this.maincontainerpanel.Size = new System.Drawing.Size(620, 430);
            this.maincontainerpanel.TabIndex = 8;
            // 
            // newOrderDiag
            // 
            this.newOrderDiag.AutoScroll = true;
            this.newOrderDiag.BackColor = System.Drawing.SystemColors.Control;
            this.newOrderDiag.BackgroundImage = global::DarziApp.Properties.Resources.diag;
            this.newOrderDiag.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.newOrderDiag.Controls.Add(this.newOrderPanel);
            this.newOrderDiag.Location = new System.Drawing.Point(0, 0);
            this.newOrderDiag.Name = "newOrderDiag";
            this.newOrderDiag.Size = new System.Drawing.Size(620, 430);
            this.newOrderDiag.TabIndex = 9;
            this.newOrderDiag.Visible = false;
            // 
            // newOrderPanel
            // 
            this.newOrderPanel.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.newOrderPanel.Controls.Add(this.label10);
            this.newOrderPanel.Controls.Add(this.btn_check);
            this.newOrderPanel.Controls.Add(this.label9);
            this.newOrderPanel.Controls.Add(this.inp_AsteenBukram);
            this.newOrderPanel.Controls.Add(this.inp_pocketsada);
            this.newOrderPanel.Controls.Add(this.inp_kurtaKallionWala);
            this.newOrderPanel.Controls.Add(this.inp_B);
            this.newOrderPanel.Controls.Add(this.inp_fullsingle);
            this.newOrderPanel.Controls.Add(this.inp_jaamSilaye);
            this.newOrderPanel.Controls.Add(this.inp_singleDhaga);
            this.newOrderPanel.Controls.Add(this.inp_L1);
            this.newOrderPanel.Controls.Add(this.inp_awaamiKurta);
            this.newOrderPanel.Controls.Add(this.inp_fullbarabar);
            this.newOrderPanel.Controls.Add(this.inp_D);
            this.newOrderPanel.Controls.Add(this.inp_Asteen);
            this.newOrderPanel.Controls.Add(this.inp_W);
            this.newOrderPanel.Controls.Add(this.inp_fullDouble);
            this.newOrderPanel.Controls.Add(this.inp_DubKanta);
            this.newOrderPanel.Controls.Add(this.inp_C);
            this.newOrderPanel.Controls.Add(this.inp_gumSilaye);
            this.newOrderPanel.Controls.Add(this.label8);
            this.newOrderPanel.Controls.Add(this.inp_readyMade);
            this.newOrderPanel.Controls.Add(this.label7);
            this.newOrderPanel.Controls.Add(this.inp_A);
            this.newOrderPanel.Controls.Add(this.inp_kaf);
            this.newOrderPanel.Controls.Add(this.label6);
            this.newOrderPanel.Controls.Add(this.inp_Romaali);
            this.newOrderPanel.Controls.Add(this.label5);
            this.newOrderPanel.Controls.Add(this.inp_S);
            this.newOrderPanel.Controls.Add(this.inp_shairwaniCollar);
            this.newOrderPanel.Controls.Add(this.label4);
            this.newOrderPanel.Controls.Add(this.inp_chamakDhaga);
            this.newOrderPanel.Controls.Add(this.label3);
            this.newOrderPanel.Controls.Add(this.inp_L);
            this.newOrderPanel.Controls.Add(this.inp_collarNookdar);
            this.newOrderPanel.Controls.Add(this.label2);
            this.newOrderPanel.Controls.Add(this.inp_doubleDhaga);
            this.newOrderPanel.Controls.Add(this.label1);
            this.newOrderPanel.Location = new System.Drawing.Point(94, 74);
            this.newOrderPanel.Name = "newOrderPanel";
            this.newOrderPanel.Size = new System.Drawing.Size(440, 301);
            this.newOrderPanel.TabIndex = 0;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(7, 7);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(15, 13);
            this.label10.TabIndex = 5;
            this.label10.Text = "X";
            this.label10.Click += new System.EventHandler(this.label10_Click);
            // 
            // btn_check
            // 
            this.btn_check.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btn_check.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_check.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_check.Location = new System.Drawing.Point(346, 261);
            this.btn_check.Name = "btn_check";
            this.btn_check.Size = new System.Drawing.Size(80, 23);
            this.btn_check.TabIndex = 47;
            this.btn_check.TabStop = false;
            this.btn_check.Text = "چیک آرڈر";
            this.btn_check.UseVisualStyleBackColor = false;
            this.btn_check.Click += new System.EventHandler(this.button1_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(54, 17);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(108, 37);
            this.label9.TabIndex = 3;
            this.label9.Text = "نیا آرڈر";
            // 
            // inp_AsteenBukram
            // 
            this.inp_AsteenBukram.AutoSize = true;
            this.inp_AsteenBukram.Location = new System.Drawing.Point(212, 249);
            this.inp_AsteenBukram.Name = "inp_AsteenBukram";
            this.inp_AsteenBukram.Size = new System.Drawing.Size(78, 17);
            this.inp_AsteenBukram.TabIndex = 37;
            this.inp_AsteenBukram.Text = "آستین بکرم";
            this.inp_AsteenBukram.UseVisualStyleBackColor = true;
            // 
            // inp_pocketsada
            // 
            this.inp_pocketsada.AutoSize = true;
            this.inp_pocketsada.Location = new System.Drawing.Point(310, 223);
            this.inp_pocketsada.Name = "inp_pocketsada";
            this.inp_pocketsada.Size = new System.Drawing.Size(99, 17);
            this.inp_pocketsada.TabIndex = 46;
            this.inp_pocketsada.Text = "پاکٹ سادہ گول";
            this.inp_pocketsada.UseVisualStyleBackColor = true;
            // 
            // inp_kurtaKallionWala
            // 
            this.inp_kurtaKallionWala.AutoSize = true;
            this.inp_kurtaKallionWala.Location = new System.Drawing.Point(212, 223);
            this.inp_kurtaKallionWala.Name = "inp_kurtaKallionWala";
            this.inp_kurtaKallionWala.Size = new System.Drawing.Size(96, 17);
            this.inp_kurtaKallionWala.TabIndex = 36;
            this.inp_kurtaKallionWala.Text = "کرتہ کلیوں والا";
            this.inp_kurtaKallionWala.UseVisualStyleBackColor = true;
            // 
            // inp_B
            // 
            this.inp_B.DecimalPlaces = 1;
            this.inp_B.Location = new System.Drawing.Point(54, 246);
            this.inp_B.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.inp_B.Name = "inp_B";
            this.inp_B.Size = new System.Drawing.Size(120, 20);
            this.inp_B.TabIndex = 27;
            this.inp_B.Enter += new System.EventHandler(this.inp_L_Enter);
            this.inp_B.MouseDown += new System.Windows.Forms.MouseEventHandler(this.inp_B_MouseDown);
            // 
            // inp_fullsingle
            // 
            this.inp_fullsingle.AutoSize = true;
            this.inp_fullsingle.Location = new System.Drawing.Point(310, 197);
            this.inp_fullsingle.Name = "inp_fullsingle";
            this.inp_fullsingle.Size = new System.Drawing.Size(102, 17);
            this.inp_fullsingle.TabIndex = 45;
            this.inp_fullsingle.Text = "فل سنگل سلائی";
            this.inp_fullsingle.UseVisualStyleBackColor = true;
            // 
            // inp_jaamSilaye
            // 
            this.inp_jaamSilaye.AutoSize = true;
            this.inp_jaamSilaye.Location = new System.Drawing.Point(212, 197);
            this.inp_jaamSilaye.Name = "inp_jaamSilaye";
            this.inp_jaamSilaye.Size = new System.Drawing.Size(75, 17);
            this.inp_jaamSilaye.TabIndex = 35;
            this.inp_jaamSilaye.Text = "جام سلائی";
            this.inp_jaamSilaye.UseVisualStyleBackColor = true;
            // 
            // inp_singleDhaga
            // 
            this.inp_singleDhaga.AutoSize = true;
            this.inp_singleDhaga.Location = new System.Drawing.Point(310, 171);
            this.inp_singleDhaga.Name = "inp_singleDhaga";
            this.inp_singleDhaga.Size = new System.Drawing.Size(85, 17);
            this.inp_singleDhaga.TabIndex = 44;
            this.inp_singleDhaga.Text = "سنگل دھاگہ";
            this.inp_singleDhaga.UseVisualStyleBackColor = true;
            // 
            // inp_L1
            // 
            this.inp_L1.DecimalPlaces = 1;
            this.inp_L1.Location = new System.Drawing.Point(54, 220);
            this.inp_L1.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.inp_L1.Name = "inp_L1";
            this.inp_L1.Size = new System.Drawing.Size(120, 20);
            this.inp_L1.TabIndex = 26;
            this.inp_L1.Enter += new System.EventHandler(this.inp_L_Enter);
            this.inp_L1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.inp_B_MouseDown);
            // 
            // inp_awaamiKurta
            // 
            this.inp_awaamiKurta.AutoSize = true;
            this.inp_awaamiKurta.Location = new System.Drawing.Point(212, 171);
            this.inp_awaamiKurta.Name = "inp_awaamiKurta";
            this.inp_awaamiKurta.Size = new System.Drawing.Size(82, 17);
            this.inp_awaamiKurta.TabIndex = 34;
            this.inp_awaamiKurta.Text = "عوامی کرتہ";
            this.inp_awaamiKurta.UseVisualStyleBackColor = true;
            // 
            // inp_fullbarabar
            // 
            this.inp_fullbarabar.AutoSize = true;
            this.inp_fullbarabar.Location = new System.Drawing.Point(310, 147);
            this.inp_fullbarabar.Name = "inp_fullbarabar";
            this.inp_fullbarabar.Size = new System.Drawing.Size(96, 17);
            this.inp_fullbarabar.TabIndex = 43;
            this.inp_fullbarabar.Text = "فل برابر سلائی";
            this.inp_fullbarabar.UseVisualStyleBackColor = true;
            // 
            // inp_D
            // 
            this.inp_D.DecimalPlaces = 1;
            this.inp_D.Location = new System.Drawing.Point(54, 196);
            this.inp_D.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.inp_D.Name = "inp_D";
            this.inp_D.Size = new System.Drawing.Size(120, 20);
            this.inp_D.TabIndex = 25;
            this.inp_D.Enter += new System.EventHandler(this.inp_L_Enter);
            this.inp_D.MouseDown += new System.Windows.Forms.MouseEventHandler(this.inp_B_MouseDown);
            // 
            // inp_Asteen
            // 
            this.inp_Asteen.AutoSize = true;
            this.inp_Asteen.Location = new System.Drawing.Point(212, 147);
            this.inp_Asteen.Name = "inp_Asteen";
            this.inp_Asteen.Size = new System.Drawing.Size(87, 17);
            this.inp_Asteen.TabIndex = 33;
            this.inp_Asteen.Text = "آستین کناری";
            this.inp_Asteen.UseVisualStyleBackColor = true;
            // 
            // inp_W
            // 
            this.inp_W.DecimalPlaces = 1;
            this.inp_W.Location = new System.Drawing.Point(54, 170);
            this.inp_W.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.inp_W.Name = "inp_W";
            this.inp_W.Size = new System.Drawing.Size(120, 20);
            this.inp_W.TabIndex = 24;
            this.inp_W.Enter += new System.EventHandler(this.inp_L_Enter);
            this.inp_W.MouseDown += new System.Windows.Forms.MouseEventHandler(this.inp_B_MouseDown);
            // 
            // inp_fullDouble
            // 
            this.inp_fullDouble.AutoSize = true;
            this.inp_fullDouble.Location = new System.Drawing.Point(310, 121);
            this.inp_fullDouble.Name = "inp_fullDouble";
            this.inp_fullDouble.Size = new System.Drawing.Size(91, 17);
            this.inp_fullDouble.TabIndex = 42;
            this.inp_fullDouble.Text = "فل ڈبل سلائی";
            this.inp_fullDouble.UseVisualStyleBackColor = true;
            // 
            // inp_DubKanta
            // 
            this.inp_DubKanta.AutoSize = true;
            this.inp_DubKanta.Location = new System.Drawing.Point(212, 121);
            this.inp_DubKanta.Name = "inp_DubKanta";
            this.inp_DubKanta.Size = new System.Drawing.Size(65, 17);
            this.inp_DubKanta.TabIndex = 32;
            this.inp_DubKanta.Text = "ڈب کانٹا";
            this.inp_DubKanta.UseVisualStyleBackColor = true;
            // 
            // inp_C
            // 
            this.inp_C.DecimalPlaces = 1;
            this.inp_C.Location = new System.Drawing.Point(54, 144);
            this.inp_C.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.inp_C.Name = "inp_C";
            this.inp_C.Size = new System.Drawing.Size(120, 20);
            this.inp_C.TabIndex = 23;
            this.inp_C.Enter += new System.EventHandler(this.inp_L_Enter);
            this.inp_C.MouseDown += new System.Windows.Forms.MouseEventHandler(this.inp_B_MouseDown);
            // 
            // inp_gumSilaye
            // 
            this.inp_gumSilaye.AutoSize = true;
            this.inp_gumSilaye.Location = new System.Drawing.Point(310, 95);
            this.inp_gumSilaye.Name = "inp_gumSilaye";
            this.inp_gumSilaye.Size = new System.Drawing.Size(71, 17);
            this.inp_gumSilaye.TabIndex = 41;
            this.inp_gumSilaye.Text = "گم سلائی";
            this.inp_gumSilaye.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(32, 248);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(17, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "B:";
            // 
            // inp_readyMade
            // 
            this.inp_readyMade.AutoSize = true;
            this.inp_readyMade.Location = new System.Drawing.Point(212, 95);
            this.inp_readyMade.Name = "inp_readyMade";
            this.inp_readyMade.Size = new System.Drawing.Size(95, 17);
            this.inp_readyMade.TabIndex = 31;
            this.inp_readyMade.Text = "ریڈی میڈ پانچہ";
            this.inp_readyMade.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(32, 222);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(22, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "L1:";
            // 
            // inp_A
            // 
            this.inp_A.DecimalPlaces = 1;
            this.inp_A.Location = new System.Drawing.Point(54, 118);
            this.inp_A.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.inp_A.Name = "inp_A";
            this.inp_A.Size = new System.Drawing.Size(120, 20);
            this.inp_A.TabIndex = 22;
            this.inp_A.Enter += new System.EventHandler(this.inp_L_Enter);
            this.inp_A.MouseDown += new System.Windows.Forms.MouseEventHandler(this.inp_B_MouseDown);
            // 
            // inp_kaf
            // 
            this.inp_kaf.AutoSize = true;
            this.inp_kaf.Location = new System.Drawing.Point(310, 69);
            this.inp_kaf.Name = "inp_kaf";
            this.inp_kaf.Size = new System.Drawing.Size(42, 17);
            this.inp_kaf.TabIndex = 40;
            this.inp_kaf.Text = "کف";
            this.inp_kaf.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(32, 198);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(18, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "D:";
            // 
            // inp_Romaali
            // 
            this.inp_Romaali.AutoSize = true;
            this.inp_Romaali.Location = new System.Drawing.Point(212, 69);
            this.inp_Romaali.Name = "inp_Romaali";
            this.inp_Romaali.Size = new System.Drawing.Size(59, 17);
            this.inp_Romaali.TabIndex = 30;
            this.inp_Romaali.Text = "رومالی";
            this.inp_Romaali.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(32, 172);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(21, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "W:";
            // 
            // inp_S
            // 
            this.inp_S.DecimalPlaces = 1;
            this.inp_S.Location = new System.Drawing.Point(54, 92);
            this.inp_S.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.inp_S.Name = "inp_S";
            this.inp_S.Size = new System.Drawing.Size(120, 20);
            this.inp_S.TabIndex = 21;
            this.inp_S.Enter += new System.EventHandler(this.inp_L_Enter);
            this.inp_S.MouseDown += new System.Windows.Forms.MouseEventHandler(this.inp_B_MouseDown);
            // 
            // inp_shairwaniCollar
            // 
            this.inp_shairwaniCollar.AutoSize = true;
            this.inp_shairwaniCollar.Location = new System.Drawing.Point(310, 43);
            this.inp_shairwaniCollar.Name = "inp_shairwaniCollar";
            this.inp_shairwaniCollar.Size = new System.Drawing.Size(89, 17);
            this.inp_shairwaniCollar.TabIndex = 39;
            this.inp_shairwaniCollar.Text = "شیروانی کالر";
            this.inp_shairwaniCollar.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(32, 146);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(17, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "C:";
            // 
            // inp_chamakDhaga
            // 
            this.inp_chamakDhaga.AutoSize = true;
            this.inp_chamakDhaga.Location = new System.Drawing.Point(212, 43);
            this.inp_chamakDhaga.Name = "inp_chamakDhaga";
            this.inp_chamakDhaga.Size = new System.Drawing.Size(77, 17);
            this.inp_chamakDhaga.TabIndex = 29;
            this.inp_chamakDhaga.Text = "چمک دھاگہ";
            this.inp_chamakDhaga.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(32, 120);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(17, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "A:";
            // 
            // inp_L
            // 
            this.inp_L.DecimalPlaces = 1;
            this.inp_L.Location = new System.Drawing.Point(54, 66);
            this.inp_L.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.inp_L.Name = "inp_L";
            this.inp_L.Size = new System.Drawing.Size(120, 20);
            this.inp_L.TabIndex = 20;
            this.inp_L.Enter += new System.EventHandler(this.inp_L_Enter);
            this.inp_L.MouseDown += new System.Windows.Forms.MouseEventHandler(this.inp_B_MouseDown);
            // 
            // inp_collarNookdar
            // 
            this.inp_collarNookdar.AutoSize = true;
            this.inp_collarNookdar.Location = new System.Drawing.Point(310, 17);
            this.inp_collarNookdar.Name = "inp_collarNookdar";
            this.inp_collarNookdar.Size = new System.Drawing.Size(78, 17);
            this.inp_collarNookdar.TabIndex = 38;
            this.inp_collarNookdar.Text = "کالر نوکدار";
            this.inp_collarNookdar.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(32, 94);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(17, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "S:";
            // 
            // inp_doubleDhaga
            // 
            this.inp_doubleDhaga.AutoSize = true;
            this.inp_doubleDhaga.Location = new System.Drawing.Point(212, 17);
            this.inp_doubleDhaga.Name = "inp_doubleDhaga";
            this.inp_doubleDhaga.Size = new System.Drawing.Size(74, 17);
            this.inp_doubleDhaga.TabIndex = 28;
            this.inp_doubleDhaga.Text = "ڈبل دھاگہ";
            this.inp_doubleDhaga.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(32, 68);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(16, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "L:";
            // 
            // viewClients
            // 
            this.viewClients.AutoScroll = true;
            this.viewClients.BackColor = System.Drawing.SystemColors.Control;
            this.viewClients.BackgroundImage = global::DarziApp.Properties.Resources.diag;
            this.viewClients.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.viewClients.Controls.Add(this.panel3);
            this.viewClients.Location = new System.Drawing.Point(0, 0);
            this.viewClients.Name = "viewClients";
            this.viewClients.Size = new System.Drawing.Size(620, 430);
            this.viewClients.TabIndex = 10;
            this.viewClients.Visible = false;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panel3.Controls.Add(this.GridViewSearch);
            this.panel3.Controls.Add(this.inp_numberSearch);
            this.panel3.Controls.Add(this.viewClientsX);
            this.panel3.Controls.Add(this.label13);
            this.panel3.Controls.Add(this.btn_search);
            this.panel3.Controls.Add(this.label12);
            this.panel3.Controls.Add(this.label11);
            this.panel3.Location = new System.Drawing.Point(94, 74);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(440, 301);
            this.panel3.TabIndex = 0;
            // 
            // GridViewSearch
            // 
            this.GridViewSearch.AllowUserToAddRows = false;
            this.GridViewSearch.AllowUserToDeleteRows = false;
            this.GridViewSearch.AllowUserToResizeColumns = false;
            this.GridViewSearch.AllowUserToResizeRows = false;
            this.GridViewSearch.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.GridViewSearch.AutoGenerateColumns = false;
            this.GridViewSearch.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            this.GridViewSearch.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridViewSearch.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nameDataGridViewTextBoxColumn,
            this.phonenumberDataGridViewTextBoxColumn,
            this.suitquatityDataGridViewTextBoxColumn,
            this.totalpaymentDataGridViewTextBoxColumn,
            this.advancepaymentDataGridViewTextBoxColumn,
            this.sizeDataGridViewTextBoxColumn,
            this.descriptionDataGridViewTextBoxColumn,
            this.orderdateDataGridViewTextBoxColumn,
            this.duedateDataGridViewTextBoxColumn,
            this.ordernumberDataGridViewTextBoxColumn,
            this.Column1});
            this.GridViewSearch.DataSource = this.ordersBindingSource1;
            this.GridViewSearch.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.GridViewSearch.GridColor = System.Drawing.SystemColors.ButtonHighlight;
            this.GridViewSearch.Location = new System.Drawing.Point(0, 69);
            this.GridViewSearch.Name = "GridViewSearch";
            this.GridViewSearch.ReadOnly = true;
            this.GridViewSearch.RowHeadersVisible = false;
            this.GridViewSearch.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.GridViewSearch.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GridViewSearch.ShowEditingIcon = false;
            this.GridViewSearch.ShowRowErrors = false;
            this.GridViewSearch.Size = new System.Drawing.Size(440, 232);
            this.GridViewSearch.TabIndex = 8;
            this.GridViewSearch.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.GridViewSearch_CellDoubleClick);
            // 
            // nameDataGridViewTextBoxColumn
            // 
            this.nameDataGridViewTextBoxColumn.DataPropertyName = "name";
            this.nameDataGridViewTextBoxColumn.HeaderText = "name";
            this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
            this.nameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // phonenumberDataGridViewTextBoxColumn
            // 
            this.phonenumberDataGridViewTextBoxColumn.DataPropertyName = "phonenumber";
            this.phonenumberDataGridViewTextBoxColumn.HeaderText = "phonenumber";
            this.phonenumberDataGridViewTextBoxColumn.Name = "phonenumberDataGridViewTextBoxColumn";
            this.phonenumberDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // suitquatityDataGridViewTextBoxColumn
            // 
            this.suitquatityDataGridViewTextBoxColumn.DataPropertyName = "suitquatity";
            this.suitquatityDataGridViewTextBoxColumn.HeaderText = "suitquatity";
            this.suitquatityDataGridViewTextBoxColumn.Name = "suitquatityDataGridViewTextBoxColumn";
            this.suitquatityDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // totalpaymentDataGridViewTextBoxColumn
            // 
            this.totalpaymentDataGridViewTextBoxColumn.DataPropertyName = "totalpayment";
            this.totalpaymentDataGridViewTextBoxColumn.HeaderText = "totalpayment";
            this.totalpaymentDataGridViewTextBoxColumn.Name = "totalpaymentDataGridViewTextBoxColumn";
            this.totalpaymentDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // advancepaymentDataGridViewTextBoxColumn
            // 
            this.advancepaymentDataGridViewTextBoxColumn.DataPropertyName = "advancepayment";
            this.advancepaymentDataGridViewTextBoxColumn.HeaderText = "advancepayment";
            this.advancepaymentDataGridViewTextBoxColumn.Name = "advancepaymentDataGridViewTextBoxColumn";
            this.advancepaymentDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // sizeDataGridViewTextBoxColumn
            // 
            this.sizeDataGridViewTextBoxColumn.DataPropertyName = "size";
            this.sizeDataGridViewTextBoxColumn.HeaderText = "size";
            this.sizeDataGridViewTextBoxColumn.Name = "sizeDataGridViewTextBoxColumn";
            this.sizeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // descriptionDataGridViewTextBoxColumn
            // 
            this.descriptionDataGridViewTextBoxColumn.DataPropertyName = "description";
            this.descriptionDataGridViewTextBoxColumn.HeaderText = "description";
            this.descriptionDataGridViewTextBoxColumn.Name = "descriptionDataGridViewTextBoxColumn";
            this.descriptionDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // orderdateDataGridViewTextBoxColumn
            // 
            this.orderdateDataGridViewTextBoxColumn.DataPropertyName = "orderdate";
            this.orderdateDataGridViewTextBoxColumn.HeaderText = "orderdate";
            this.orderdateDataGridViewTextBoxColumn.Name = "orderdateDataGridViewTextBoxColumn";
            this.orderdateDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // duedateDataGridViewTextBoxColumn
            // 
            this.duedateDataGridViewTextBoxColumn.DataPropertyName = "duedate";
            this.duedateDataGridViewTextBoxColumn.HeaderText = "duedate";
            this.duedateDataGridViewTextBoxColumn.Name = "duedateDataGridViewTextBoxColumn";
            this.duedateDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // ordernumberDataGridViewTextBoxColumn
            // 
            this.ordernumberDataGridViewTextBoxColumn.DataPropertyName = "ordernumber";
            this.ordernumberDataGridViewTextBoxColumn.HeaderText = "ordernumber";
            this.ordernumberDataGridViewTextBoxColumn.Name = "ordernumberDataGridViewTextBoxColumn";
            this.ordernumberDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "Id";
            this.Column1.HeaderText = "Column1";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Visible = false;
            // 
            // ordersBindingSource1
            // 
            this.ordersBindingSource1.DataMember = "Orders";
            this.ordersBindingSource1.DataSource = this.database1DataSet;
            // 
            // database1DataSet
            // 
            this.database1DataSet.DataSetName = "Database1DataSet";
            this.database1DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // inp_numberSearch
            // 
            this.inp_numberSearch.CutCopyMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            this.inp_numberSearch.Location = new System.Drawing.Point(204, 37);
            this.inp_numberSearch.Mask = "(9999) 000-0000";
            this.inp_numberSearch.Name = "inp_numberSearch";
            this.inp_numberSearch.Size = new System.Drawing.Size(136, 20);
            this.inp_numberSearch.TabIndex = 7;
            this.inp_numberSearch.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // viewClientsX
            // 
            this.viewClientsX.AutoSize = true;
            this.viewClientsX.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.viewClientsX.Location = new System.Drawing.Point(7, 7);
            this.viewClientsX.Name = "viewClientsX";
            this.viewClientsX.Size = new System.Drawing.Size(15, 13);
            this.viewClientsX.TabIndex = 6;
            this.viewClientsX.Text = "X";
            this.viewClientsX.Click += new System.EventHandler(this.viewClientsX_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(31, 44);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(150, 13);
            this.label13.TabIndex = 4;
            this.label13.Text = "Find Specific Orders Of Clients";
            // 
            // btn_search
            // 
            this.btn_search.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btn_search.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_search.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_search.Location = new System.Drawing.Point(346, 18);
            this.btn_search.Name = "btn_search";
            this.btn_search.Size = new System.Drawing.Size(80, 42);
            this.btn_search.TabIndex = 3;
            this.btn_search.TabStop = false;
            this.btn_search.Text = "View Client";
            this.btn_search.UseVisualStyleBackColor = false;
            this.btn_search.Click += new System.EventHandler(this.btn_search_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(202, 18);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(138, 13);
            this.label12.TabIndex = 1;
            this.label12.Text = "Enter Client Mobile Number:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(28, 14);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(162, 20);
            this.label11.TabIndex = 0;
            this.label11.Text = "View Client Orders:";
            // 
            // viewAllTrans
            // 
            this.viewAllTrans.AutoScroll = true;
            this.viewAllTrans.BackColor = System.Drawing.SystemColors.Control;
            this.viewAllTrans.BackgroundImage = global::DarziApp.Properties.Resources.diag;
            this.viewAllTrans.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.viewAllTrans.Controls.Add(this.panel4);
            this.viewAllTrans.Location = new System.Drawing.Point(-2, 0);
            this.viewAllTrans.Name = "viewAllTrans";
            this.viewAllTrans.Size = new System.Drawing.Size(620, 430);
            this.viewAllTrans.TabIndex = 11;
            this.viewAllTrans.Visible = false;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panel4.Controls.Add(this.closeOrders);
            this.panel4.Controls.Add(this.GridViewAll);
            this.panel4.Controls.Add(this.label17);
            this.panel4.Location = new System.Drawing.Point(94, 74);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(440, 301);
            this.panel4.TabIndex = 0;
            // 
            // closeOrders
            // 
            this.closeOrders.AutoSize = true;
            this.closeOrders.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.closeOrders.Location = new System.Drawing.Point(7, 7);
            this.closeOrders.Name = "closeOrders";
            this.closeOrders.Size = new System.Drawing.Size(15, 13);
            this.closeOrders.TabIndex = 6;
            this.closeOrders.Text = "X";
            this.closeOrders.Click += new System.EventHandler(this.closeOrders_Click);
            // 
            // GridViewAll
            // 
            this.GridViewAll.AllowUserToAddRows = false;
            this.GridViewAll.AllowUserToDeleteRows = false;
            this.GridViewAll.AllowUserToResizeColumns = false;
            this.GridViewAll.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.GridViewAll.AutoGenerateColumns = false;
            this.GridViewAll.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            this.GridViewAll.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridViewAll.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn1,
            this.ordernumberDataGridViewTextBoxColumn1,
            this.phonenumberDataGridViewTextBoxColumn1,
            this.suitquatityDataGridViewTextBoxColumn1,
            this.totalpaymentDataGridViewTextBoxColumn1,
            this.advancepaymentDataGridViewTextBoxColumn1,
            this.sizeDataGridViewTextBoxColumn1,
            this.descriptionDataGridViewTextBoxColumn1,
            this.orderdateDataGridViewTextBoxColumn1,
            this.duedateDataGridViewTextBoxColumn1,
            this.nameDataGridViewTextBoxColumn1});
            this.GridViewAll.DataSource = this.ordersBindingSource1;
            this.GridViewAll.Location = new System.Drawing.Point(0, 40);
            this.GridViewAll.Name = "GridViewAll";
            this.GridViewAll.ReadOnly = true;
            this.GridViewAll.RowHeadersVisible = false;
            this.GridViewAll.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GridViewAll.Size = new System.Drawing.Size(440, 244);
            this.GridViewAll.TabIndex = 5;
            // 
            // idDataGridViewTextBoxColumn1
            // 
            this.idDataGridViewTextBoxColumn1.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn1.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn1.Name = "idDataGridViewTextBoxColumn1";
            this.idDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // ordernumberDataGridViewTextBoxColumn1
            // 
            this.ordernumberDataGridViewTextBoxColumn1.DataPropertyName = "ordernumber";
            this.ordernumberDataGridViewTextBoxColumn1.HeaderText = "ordernumber";
            this.ordernumberDataGridViewTextBoxColumn1.Name = "ordernumberDataGridViewTextBoxColumn1";
            this.ordernumberDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // phonenumberDataGridViewTextBoxColumn1
            // 
            this.phonenumberDataGridViewTextBoxColumn1.DataPropertyName = "phonenumber";
            this.phonenumberDataGridViewTextBoxColumn1.HeaderText = "phonenumber";
            this.phonenumberDataGridViewTextBoxColumn1.Name = "phonenumberDataGridViewTextBoxColumn1";
            this.phonenumberDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // suitquatityDataGridViewTextBoxColumn1
            // 
            this.suitquatityDataGridViewTextBoxColumn1.DataPropertyName = "suitquatity";
            this.suitquatityDataGridViewTextBoxColumn1.HeaderText = "suitquatity";
            this.suitquatityDataGridViewTextBoxColumn1.Name = "suitquatityDataGridViewTextBoxColumn1";
            this.suitquatityDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // totalpaymentDataGridViewTextBoxColumn1
            // 
            this.totalpaymentDataGridViewTextBoxColumn1.DataPropertyName = "totalpayment";
            this.totalpaymentDataGridViewTextBoxColumn1.HeaderText = "totalpayment";
            this.totalpaymentDataGridViewTextBoxColumn1.Name = "totalpaymentDataGridViewTextBoxColumn1";
            this.totalpaymentDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // advancepaymentDataGridViewTextBoxColumn1
            // 
            this.advancepaymentDataGridViewTextBoxColumn1.DataPropertyName = "advancepayment";
            this.advancepaymentDataGridViewTextBoxColumn1.HeaderText = "advancepayment";
            this.advancepaymentDataGridViewTextBoxColumn1.Name = "advancepaymentDataGridViewTextBoxColumn1";
            this.advancepaymentDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // sizeDataGridViewTextBoxColumn1
            // 
            this.sizeDataGridViewTextBoxColumn1.DataPropertyName = "size";
            this.sizeDataGridViewTextBoxColumn1.HeaderText = "size";
            this.sizeDataGridViewTextBoxColumn1.Name = "sizeDataGridViewTextBoxColumn1";
            this.sizeDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // descriptionDataGridViewTextBoxColumn1
            // 
            this.descriptionDataGridViewTextBoxColumn1.DataPropertyName = "description";
            this.descriptionDataGridViewTextBoxColumn1.HeaderText = "description";
            this.descriptionDataGridViewTextBoxColumn1.Name = "descriptionDataGridViewTextBoxColumn1";
            this.descriptionDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // orderdateDataGridViewTextBoxColumn1
            // 
            this.orderdateDataGridViewTextBoxColumn1.DataPropertyName = "orderdate";
            this.orderdateDataGridViewTextBoxColumn1.HeaderText = "orderdate";
            this.orderdateDataGridViewTextBoxColumn1.Name = "orderdateDataGridViewTextBoxColumn1";
            this.orderdateDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // duedateDataGridViewTextBoxColumn1
            // 
            this.duedateDataGridViewTextBoxColumn1.DataPropertyName = "duedate";
            this.duedateDataGridViewTextBoxColumn1.HeaderText = "duedate";
            this.duedateDataGridViewTextBoxColumn1.Name = "duedateDataGridViewTextBoxColumn1";
            this.duedateDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // nameDataGridViewTextBoxColumn1
            // 
            this.nameDataGridViewTextBoxColumn1.DataPropertyName = "name";
            this.nameDataGridViewTextBoxColumn1.HeaderText = "name";
            this.nameDataGridViewTextBoxColumn1.Name = "nameDataGridViewTextBoxColumn1";
            this.nameDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(28, 14);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(162, 20);
            this.label17.TabIndex = 0;
            this.label17.Text = "View Client Orders:";
            // 
            // ordersBindingSource
            // 
            this.ordersBindingSource.DataMember = "Orders";
            // 
            // ordersTableAdapter
            // 
            this.ordersTableAdapter.ClearBeforeFill = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = global::DarziApp.Properties.Resources.main;
            this.ClientSize = new System.Drawing.Size(784, 511);
            this.Controls.Add(this.menupanel);
            this.Controls.Add(this.mainmenustrip);
            this.Controls.Add(this.navigationpanel);
            this.Controls.Add(this.maincontainerpanel);
            this.Controls.Add(this.bottomstrip);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "Tanoli Brothers";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.navigationpanel.ResumeLayout(false);
            this.navigationpanel.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.bottomstrip.ResumeLayout(false);
            this.bottomstrip.PerformLayout();
            this.mainmenustrip.ResumeLayout(false);
            this.mainmenustrip.PerformLayout();
            this.menupanel.ResumeLayout(false);
            this.maincontainerpanel.ResumeLayout(false);
            this.newOrderDiag.ResumeLayout(false);
            this.newOrderPanel.ResumeLayout(false);
            this.newOrderPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.inp_B)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inp_L1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inp_D)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inp_W)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inp_C)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inp_A)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inp_S)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inp_L)).EndInit();
            this.viewClients.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridViewSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ordersBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.database1DataSet)).EndInit();
            this.viewAllTrans.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridViewAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ordersBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStripMenuItem helpContentToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem aboutSoftwareToolStripMenuItem;
        private System.Windows.Forms.Panel menupanel;
        private System.Windows.Forms.Button btn_new;
        private System.Windows.Forms.Button btn_history;
        private System.Windows.Forms.Button btn_home;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.Panel navigationpanel;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripSplitButton toolStripSplitButton1;
        private System.Windows.Forms.Panel maincontainerpanel;
        public System.Windows.Forms.StatusStrip bottomstrip;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.MenuStrip mainmenustrip;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem1;
        private System.Windows.Forms.Button btn_view;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel newOrderDiag;
        private System.Windows.Forms.Panel newOrderPanel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown inp_L;
        private System.Windows.Forms.CheckBox inp_doubleDhaga;
        private System.Windows.Forms.CheckBox inp_AsteenBukram;
        private System.Windows.Forms.CheckBox inp_pocketsada;
        private System.Windows.Forms.CheckBox inp_kurtaKallionWala;
        private System.Windows.Forms.NumericUpDown inp_B;
        private System.Windows.Forms.CheckBox inp_fullsingle;
        private System.Windows.Forms.CheckBox inp_jaamSilaye;
        private System.Windows.Forms.CheckBox inp_singleDhaga;
        private System.Windows.Forms.NumericUpDown inp_L1;
        private System.Windows.Forms.CheckBox inp_awaamiKurta;
        private System.Windows.Forms.CheckBox inp_fullbarabar;
        private System.Windows.Forms.NumericUpDown inp_D;
        private System.Windows.Forms.CheckBox inp_Asteen;
        private System.Windows.Forms.NumericUpDown inp_W;
        private System.Windows.Forms.CheckBox inp_fullDouble;
        private System.Windows.Forms.CheckBox inp_DubKanta;
        private System.Windows.Forms.NumericUpDown inp_C;
        private System.Windows.Forms.CheckBox inp_gumSilaye;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox inp_readyMade;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown inp_A;
        private System.Windows.Forms.CheckBox inp_kaf;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox inp_Romaali;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown inp_S;
        private System.Windows.Forms.CheckBox inp_shairwaniCollar;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox inp_chamakDhaga;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox inp_collarNookdar;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btn_check;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel viewClients;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button btn_search;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.BindingSource ordersBindingSource;
        private System.Windows.Forms.Label viewClientsX;
        private System.Windows.Forms.MaskedTextBox inp_numberSearch;
        private System.Windows.Forms.Panel viewAllTrans;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label closeOrders;
        private System.Windows.Forms.DataGridView GridViewAll;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.DataGridView GridViewSearch;
        private Database1DataSet database1DataSet;
        private System.Windows.Forms.BindingSource ordersBindingSource1;
        private Database1DataSetTableAdapters.OrdersTableAdapter ordersTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ordernumberDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn phonenumberDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn suitquatityDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn totalpaymentDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn advancepaymentDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn sizeDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn descriptionDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn orderdateDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn duedateDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn phonenumberDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn suitquatityDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn totalpaymentDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn advancepaymentDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sizeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn descriptionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn orderdateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn duedateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ordernumberDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
       // private DarziApp.MainDatabaseDataSet mainDatabaseDataSet;
       // private DarziApp.MainDatabaseDataSetTableAdapters.OrdersTableAdapter ordersTableAdapter;
    }
}

